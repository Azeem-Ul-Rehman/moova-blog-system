<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',

            'page-list',
            'page-create',
            'page-edit',
            'page-delete',

            'posts-list',
            'posts-create',
            'posts-edit',
            'posts-delete',

            'category-list',
            'category-create',
            'category-edit',
            'category-delete',

            'tag-list',
            'tag-create',
            'tag-edit',
            'tag-delete',


            'gym-category-list',
            'gym-category-create',
            'gym-category-edit',
            'gym-category-delete',

            'gym-list',
            'gym-create',
            'gym-edit',
            'gym-delete',

            'instructor-list',
            'instructor-create',
            'instructor-edit',
            'instructor-delete',

            'reviews-list',
            'reviews-create',
            'reviews-edit',
            'reviews-delete'
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
