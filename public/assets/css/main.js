$(document).ready(function() {

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
            "<i class='fa fa-caret-left'></i>",
            "<i class='fa fa-caret-right'></i>"
        ],
        autoplay: 1000,
        responsiveBaseElement: 'body',
        autoplayHoverPause: true,
        responsiveClass:true,
        items : 1, // THIS IS IMPORTANT
         responsive : {
            480 : { items : 1  }, // from zero to 480 screen width 4 items
            768 : { items : 2  }, // from 480 screen widthto 768 6 items
            1024 : { items : 3   // from 768 screen width to 1024 8 items
            }
        },
    });

    
 
   
  });

  $(".pricing5 .btn-group .btn").click(function() {
    $(".pricing5 .monthly, .pricing5 .yearly").toggle();
  });