<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GymCategory extends Model
{
    use HasFactory;

    public function gyms()
    {
        return $this->belongsToMany(Gym::class, 'category_gyms','category_id','gym_id')->orderBy('created_at', 'DESC')->paginate(5);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
