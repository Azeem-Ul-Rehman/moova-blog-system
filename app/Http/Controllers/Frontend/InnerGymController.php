<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gym;

class InnerGymController extends Controller
{
    public function index()
    {
        return view('frontend.pages.inner-gym');
    }


}
