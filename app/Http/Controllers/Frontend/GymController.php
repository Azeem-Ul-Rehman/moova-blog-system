<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ClassSchedule;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Models\Gym;
use App\Models\GymCategory;
use Illuminate\Support\Facades\DB;

class GymController extends Controller
{
    public function index()
    {
        $gyms = Gym::with(['categories'])->get();
        $categories = GymCategory::where('type', 'gym')->get();
        return view('frontend.pages.gyms', compact('gyms', 'categories'));
    }

    public function category($id)
    {
        $gyms = Gym::whereHas('categories', function ($q) use ($id) {
            $q->where('id', $id);
        })->get();
        $categories = GymCategory::where('type', 'gym')->get();

        return view('frontend.pages.gyms', compact('gyms', 'categories', 'id'));
    }

    public function show($id)
    {
        $gym = Gym::find($id);
        $classes = DB::table('class_schedules')->select('*')->where('gym_id', $id)->groupBy(DB::raw('(class_id)'))->groupBy('class_id')->get();

        $availableDates = DB::table('class_schedules')->select('*')->where('gym_id', $id)->groupBy(DB::raw('DATE(start_date)'))->groupBy('start_date')->get();
        $type = 'gym';

        $reviewCount = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'gym')->count();
        $reviews = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'gym')->get();
        $reviewStars = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'gym')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

        return view('frontend.pages.inner-gym', compact('gym', 'reviewStars', 'reviewCount', 'reviews', 'classes', 'gym', 'type', 'availableDates'));
    }
}
