<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ClassSchedule;
use App\Models\Gym;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Instructor;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{


    public function index()
    {
        $posts = Post::with(['categories', 'tags'])->get();
        $gyms = Gym::with(['categories'])->where('is_featured', true)->latest('created_at')->limit(4)->get();

        $trainers = Instructor::all();
        $reviews = Review::where('status', 'approved')->take(2)->get();

        return view('frontend.welcome', compact('posts', 'trainers', 'gyms', 'reviews'));
    }

    public function show($id)
    {
        $instructor = Instructor::find($id);
        $type = 'instructor';
        $classes = DB::table('class_schedules')->select('*')->where('instructor_id', $id)->groupBy(DB::raw('(class_id)'))->groupBy('class_id')->get();
        $availableDates = DB::table('class_schedules')->select('*')->where('instructor_id', $id)->groupBy(DB::raw('DATE(start_date)'))->groupBy('start_date')->get();


        $reviewCount = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'instructor')->count();
        $reviews = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'instructor')->get();
        $reviewStars = Review::where('type_id', $id)->where('status', 'approved')->where('type', 'instructor')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

        return view('frontend.pages.trainer-detail', compact('instructor', 'reviewCount', 'reviewStars', 'reviews', 'classes', 'type','availableDates'));
    }

    public function classScheduleDetail(Request $request)
    {
        $type = $request->type;

        if ($type == 'gym') {
            $classes = ClassSchedule::where('class_id', $request->class_id)->where('gym_id', $request->id)->with(['gym', 'uClass', 'classScheduleTime'])->get();
        } else {
            $classes = ClassSchedule::where('class_id', $request->class_id)->where('instructor_id', $request->id)->with(['instructor', 'uClass', 'classScheduleTime'])->get();
        }

        $data = view('frontend.sections.class-schedule-detail', compact('type', 'classes'))->render();
        return response()->json(['options' => $data]);
    }

    public function classScheduleDetailDate(Request $request)
    {
        $type = $request->type;

        if ($type == 'gym') {
            $classes = ClassSchedule::where('gym_id', $request->id)->whereDate('start_date', $request->date)->with(['gym', 'uClass', 'classScheduleTime','instructor'])->get();
        } else {
            $classes = ClassSchedule::where('instructor_id', $request->id)->whereDate('start_date', $request->date)->with(['gym','instructor', 'uClass', 'classScheduleTime'])->get();
        }

        $data = view('frontend.sections.class-schedule-detail', compact('type', 'classes'))->render();
        return response()->json(['options' => $data]);
    }
}
