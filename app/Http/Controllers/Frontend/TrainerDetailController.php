<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Instructor;

class TrainerDetailController extends Controller
{
    public function index()
    {
        $instructor = Instructor::first();

        return view('frontend.pages.trainer-detail',compact('instructor'));
    }

}
