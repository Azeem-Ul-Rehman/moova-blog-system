<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;

class PageController extends Controller
{

    public function show($slug)
    {
        $page = Page::where('slug', $slug)->first();
        return view('frontend.pages.about', compact('page'));
    }

}
