<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gym;
use App\Models\Instructor;
use App\Models\Post;
use App\Models\Review;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{


    public function index()
    {
        if (auth()->check()) {
            $reviewsCount = Review::count();
            $usersCount = User::count();
            $postCount = Post::count();
            $gymCount = Gym::count();
            $instructorCount = Instructor::count();
            return view('admin.home', compact('reviewsCount', 'postCount', 'usersCount','gymCount','instructorCount'));

        } else {
            return redirect()->route('home.index')->with('message', 'Unauthenticated.');
        }
    }
}
