<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:users-list|users-create|users-edit|users-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check()) {
            if (auth()->user()->is_admin == true) {
                $users = User::orderBy('created_at', 'DESC')->get();
            } else {
                $users = User::orderBy('created_at', 'DESC')->where('id', auth()->user()->id)->get();
            }

            return view('admin.user.index', compact('users'));
        } else {
            return redirect()->route('home.index')->with('message', 'Unauthenticated.');


        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->check()) {
            $roles = Role::pluck('name', 'name')->all();
            return view('admin.user.create', compact('roles'));
        } else {
            return redirect()->route('home.index')->with('message', 'Unauthenticated.');


        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
//            'address' => 'required',
//            'age' => 'required',
            'roles' => 'required'
        ]);


        $user = User::create([
            'name' => trim($request->input('name')),
            'email' => strtolower($request->input('email')),
            'password' => bcrypt($request->input('password')),
            'address' => $request->address,
            'age' => $request->age,
            'status' => $request->status,
        ]);

        $user->assignRole($request->input('roles'));
        return redirect(route('user.index'))->with('message', 'Added User Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->check()) {
            $user = User::where('id', $id)->first();
            $roles = Role::pluck('name', 'name')->all();
            $userRole = $user->roles->pluck('name', 'name')->all();
            return view('admin.user.edit', compact('user', 'roles', 'userRole'));
        } else {
            return redirect()->route('home.index')->with('message', 'Unauthenticated.');

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
//            'address' => 'required',
//            'age' => 'required',
            'roles' => Rule::requiredIf(function () use ($request) {
                if (auth()->user()->is_admin == true) {
                    return true;
                }
            }),
        ]);
        $user = User::find($id);
        if (isset($request->password) && !is_null($request->password)) {
            $user->password = $request->password;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->age = $request->age;
        $user->status = $request->status;
        $user->save();
        if (auth()->user()->is_admin == true) {
            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $user->assignRole($request->input('roles'));
        }
        return redirect(route('user.index'))->with('message', 'Updated User Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect(route('user.index'))->with('message', 'Deleted User Successfully!!!!');
    }
}
