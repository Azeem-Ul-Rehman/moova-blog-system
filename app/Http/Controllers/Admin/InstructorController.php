<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Instructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:instructor-list|instructor-create|instructor-edit|instructor-delete', ['only' => ['index','store']]);
        $this->middleware('permission:instructor-create', ['only' => ['create','store']]);
        $this->middleware('permission:instructor-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:instructor-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructors = Instructor::orderBy('created_at', 'DESC')->get();
        return view('admin.instructor.index', compact('instructors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.instructor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'about' => 'required',
            'image' => 'nullable|max:1999',
        ]);
//        dd($request->all());
        //Handle file upload
        if ($request->hasFile('image')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        //Create new Gym
        $instructor = new Instructor();
        $instructor->image = $fileNameToStore;
        $instructor->name = $request->name;
        $instructor->description = $request->description;
        $instructor->about = $request->about;
        $instructor->user_name = auth()->user()->name;
        $instructor->save();
        return redirect(route('instructor.index'))->with('message', 'Added Instructor Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instructor = Instructor::where('id', $id)->first();

        return view('admin.instructor.edit', compact('instructor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'about' => 'required',
            'image' => 'nullable|max:1999'
        ]);
        //Handle file upload
        if ($request->hasFile('image')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        //Update file
        $instructor = Instructor::find($id);
        $instructor->name = $request->name;
        $instructor->description = $request->description;
        $instructor->about = $request->about;
        $instructor->user_name = auth()->user()->name;
        if ($request->hasFile('image')) {
            // Delete the old image if it's changed .
            if ($instructor->image != 'no_image.png') {
                Storage::delete('public/images/' . $instructor->image);
            }
            $instructor->image = $fileNameToStore;
        }
        $instructor->save();
        return redirect(route('instructor.index'))->with('message', 'Updated Instructor Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instructor = Instructor::find($id);
        //Delete image from gym
        if ($instructor->image != 'noimage.jpg') {
            Storage::delete('public/images/' . $instructor->image);
        }
        $instructor->delete();
        return redirect(route('instructor.index'))->with('message', 'Deleted Instructor Successfully!!!!');
    }
}
