<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:reviews-list|reviews-create|reviews-edit|reviews-delete', ['only' => ['index','store']]);
        $this->middleware('permission:reviews-create', ['only' => ['create','store']]);
        $this->middleware('permission:reviews-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:reviews-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::orderBy('created_at', 'DESC')->get();
        return view('admin.reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'type_id' => 'required',
            'type' => 'required',
            'rating' => 'required',
            'title' => 'required',
            'description' => 'required',
        ], [
            'user_id.required' => 'User must be login.',
            'type_id.required' => 'Type is required.',
            'rating.required' => 'Rating is required.',
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
        ]);
        $review = new Review();
        $review->user_id = $request->user_id;
        $review->type_id = $request->type_id;
        $review->type = $request->type;
        $review->rating = $request->rating;
        $review->title = $request->title;
        $review->description = $request->description;
        $review->save();
        return redirect()->back()->with('message', 'Review Added Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::where('id', $id)->first();
        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $review = Review::find($id);
        $this->validate($request, [
            'user_id' => 'required',
            'type_id' => 'required',
            'type' => 'required',
            'title' => 'required',
            'description' => 'required',
        ], [
            'user_id.required' => 'User must be login.',
            'type_id.required' => 'Type is required.',
            'rating.required' => 'Rating is required.',
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
        ]);

        $review->user_id = $request->user_id;
        $review->type_id = $request->type_id;
        $review->type = $request->type;
        if(isset($request->rating)){
            $review->rating = $request->rating;
        }
        $review->status = $request->status;
        $review->title = $request->title;
        $review->description = $request->description;
        $review->save();
        return redirect(route('reviews.index'))->with('message', 'Updated Review Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::find($id);
        $review->delete();
        return redirect(route('reviews.index'))->with('message', 'Deleted Review Successfully!!!!');
    }

}
