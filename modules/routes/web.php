<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Http\Controllers\Frontend as Frontend;
use App\Http\Controllers\Auth as Auth;


Route::get('/', [Frontend\HomeController::class, 'index'])->name('home.index');
Route::get('about', [Frontend\AboutController::class, 'index'])->name('about.index');
Route::get('page/{slug}', [Frontend\PageController::class, 'show'])->name('user.page.index');
Route::get('explore', [Frontend\ExploreController::class, 'index'])->name('explore.index');
Route::get('gym', [Frontend\GymController::class, 'index'])->name('user.gym.index');
Route::get('how-it-works', [Frontend\HowItWorksController::class, 'index'])->name('how-it-works.index');
Route::get('inner-gym', [Frontend\InnerGymController::class, 'index'])->name('inner-gym.index');
Route::get('join-us', [Frontend\JoinUsController::class, 'index'])->name('join-us.index');
Route::get('pricing', [Frontend\PricingController::class, 'index'])->name('pricing.index');
Route::get('trainer-detail', [Frontend\TrainerDetailController::class, 'index'])->name('trainer-detail.index');

Route::get('blog', [Frontend\blogController::class, 'index'])->name('blog.index');
Route::get('single-blog/{id}', [Frontend\blogController::class, 'show'])->name('single-blog.show');
Route::get('blog/category/{id}', [Frontend\blogController::class, 'category'])->name('blog.category');
Route::get('blog/tag/{id}', [Frontend\blogController::class, 'tags'])->name('blog.tag');

Route::get('gym/category/{id}', [Frontend\GymController::class, 'category'])->name('gym.category');
Route::get('inner-gym/{id}', [Frontend\GymController::class, 'show'])->name('inner-gym.show');


Route::get('trainer-detail/{id}', [Frontend\HomeController::class, 'show'])->name('trainer-detail.show');

Route::get('/register', [Auth\RegisterController::class, 'create'])->name('register.create');
Route::post('register', [Auth\RegisterController::class, 'store'])->name('register.store');

Route::get('/login', [Auth\LoginController::class, 'create'])->name('login.create');
Route::post('login-store', [Auth\LoginController::class, 'store'])->name('login.store');
Route::get('/logout', [Auth\LoginController::class, 'logout'])->name('logout');


Route::post('review', [Frontend\ReviewController::class, 'store'])->name('review.store');

Route::post('class-schedule/detail',[Frontend\HomeController::class,'classScheduleDetail'])->name('class-schedule.detail');

Route::post('class-schedule/detail/date',[Frontend\HomeController::class,'classScheduleDetailDate'])->name('class-schedule.detail.date');

//All Admin Routes


use App\Http\Controllers\Admin as Admin;

Route::get('admin/home', [Admin\HomeController::class, 'index'])->name('admin.home');
Route::resource('admin/category', Admin\CategoryController::class);
Route::resource('admin/roles', Admin\RoleController::class);
Route::resource('admin/user', Admin\UserController::class);
Route::resource('admin/gym-category', Admin\GymCategoryController::class);
Route::resource('admin/tag', Admin\TagController::class);
Route::resource('admin/post', Admin\PostController::class);
Route::resource('admin/gym', Admin\GymController::class);
Route::resource('admin/instructor', Admin\InstructorController::class);
Route::resource('admin/page', Admin\PageController::class);
Route::resource('admin/reviews', Admin\ReviewController::class);
Route::resource('admin/classes', Admin\UClassController::class);
Route::resource('admin/class-schedule', Admin\ClassScheduleController::class);

Route::get('admin/role/matrix', [Admin\RoleController::class,'rolesPermissions'])->name('admin.roles.matrix.index');

Route::post('admin/role/matrix/store', [Admin\RoleController::class,'rolesPermissionsStore'])->name('admin.roles.matrix.store');
