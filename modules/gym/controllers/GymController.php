<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gym;
use App\Models\GymCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GymController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:gym-list|gym-create|gym-edit|gym-delete', ['only' => ['index','store']]);
        $this->middleware('permission:gym-create', ['only' => ['create','store']]);
        $this->middleware('permission:gym-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:gym-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gyms = Gym::orderBy('created_at', 'DESC')->get();
        return view('admin.gym.index', compact('gyms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = GymCategory::where('type', 'gym')->get();
        return view('admin.gym.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'image' => 'nullable|max:1999',
            'image_thumbnail' => 'nullable|max:1999',
        ]);
//        dd($request->all());
        //Handle file upload
        if ($request->hasFile('image')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }


        if ($request->hasFile('image_thumbnail')) {
            // Get filename with the extension
            $filenameWithExt1 = $request->file('image_thumbnail')->getClientOriginalName();
            // Get just filename
            $filename1 = pathinfo($filenameWithExt1, PATHINFO_FILENAME);
            // Get just ext
            $extension1 = $request->file('image_thumbnail')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStoreThumbail = $filename1 . '_' . time() . '.' . $extension1;
            // Upload image
            $path1 = $request->file('image_thumbnail')->storeAs('public/image_thumbnail', $fileNameToStoreThumbail);
        } else {
            $fileNameToStoreThumbail = 'noimage.jpg';
        }

        //Create new Gym
        $gym = new Gym();
        $gym->image = $fileNameToStore;
        $gym->image_thumbnail = $fileNameToStoreThumbail;
        $gym->name = $request->name;
        $gym->address = $request->address;
        $gym->description = $request->description;
        $gym->is_featured = $request->is_featured;
        $gym->save();

//        foreach ($request->categories as $cate) {
//            $gymCat = new GymCategory();
//            $gymCat->category_id = $cate;
//            $gymCat->gym_id = $gym->id;
//
//        }
        // Many to many relation between Gyms and Categories
        $gym->categories()->sync($request->categories);
        return redirect(route('gym.index'))->with('message', 'Added Gym Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gym = Gym::with('categories')->where('id', $id)->first();
        $categories = GymCategory::where('type', 'gym')->get();
        return view('admin.gym.edit', compact('gym', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'image' => 'nullable|max:1999',
            'image_thumbnail' => 'nullable|max:1999',
        ]);
        //Handle file upload
        if ($request->hasFile('image')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        if ($request->hasFile('image_thumbnail')) {
            // Get filename with the extension
            $filenameWithExt1 = $request->file('image_thumbnail')->getClientOriginalName();
            // Get just filename
            $filename1 = pathinfo($filenameWithExt1, PATHINFO_FILENAME);
            // Get just ext
            $extension1 = $request->file('image_thumbnail')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStoreThumbail = $filename1 . '_' . time() . '.' . $extension1;
            // Upload image
            $path1 = $request->file('image_thumbnail')->storeAs('public/image_thumbnail', $fileNameToStoreThumbail);
        } else {
            $fileNameToStoreThumbail = 'noimage.jpg';
        }

        //Update file
        $gym = Gym::find($id);
        $gym->name = $request->name;
        $gym->address = $request->address;
        $gym->description = $request->description;
        $gym->is_featured = $request->is_featured;
        if ($request->hasFile('image')) {
            // Delete the old image if it's changed .
            if ($gym->image != 'no_image.png') {
                Storage::delete('public/images/' . $gym->image);
            }
            $gym->image = $fileNameToStore;
        }

        if ($request->hasFile('image_thumbnail')) {
            // Delete the old image if it's changed .
            if ($gym->image_thumbnail != 'no_image.png') {
                Storage::delete('public/images/' . $gym->image_thumbnail);
            }
            $gym->image_thumbnail = $fileNameToStoreThumbail;
        }

        $gym->categories()->sync($request->categories);
        $gym->save();

        return redirect(route('gym.index'))->with('message', 'Updated Gym Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gym = Gym::find($id);
        //Delete image from gym
        if ($gym->image != 'noimage.jpg') {
            Storage::delete('public/images/' . $gym->image);
        }
//        $gym->categories()->detach();
        $gym->delete();
        return redirect(route('gym.index'))->with('message', 'Deleted Gym Successfully!!!!');
    }
}
