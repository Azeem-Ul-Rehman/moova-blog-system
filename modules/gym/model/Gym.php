<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gym extends Model
{
    use HasFactory;

    public function categories()
    {
        return $this->belongsToMany(GymCategory::class, 'category_gyms', 'category_id', 'gym_id')->withTimestamps();
    }

    public function classSchedule()
    {
        return $this->hasOne(ClassSchedule::class, 'gym_id', 'id');
    }


}
