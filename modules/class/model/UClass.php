<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UClass extends Model
{
    use HasFactory;

    public function classSchedule()
    {
        return $this->hasOne(ClassSchedule::class, 'class_id', 'id');
    }
}
