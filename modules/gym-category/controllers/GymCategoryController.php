<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\GymCategory;
use Illuminate\Http\Request;

class GymCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:gym-category-list|gym-category-create|gym-category-edit|gym-category-delete', ['only' => ['index','store']]);
        $this->middleware('permission:gym-category-create', ['only' => ['create','store']]);
        $this->middleware('permission:gym-category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:gym-category-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = GymCategory::orderBy('created_at','DESC')->get();
        return view('admin.gym-category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gym-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'required',
            'type' => 'required',
        ]);
        $category = new GymCategory;
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->type = $request->type;
        $category->save();
        return redirect(route('gym-category.index'))->with('message', 'Added Gym Category Successfully!!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = GymCategory::where('id',$id)->first();
        return view('admin.gym-category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'slug' => 'required',
            'type' => 'required',
        ]);
        $category = GymCategory::find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->type = $request->type;
        $category->save();

        return redirect(route('gym-category.index'))->with('message', 'Edited Gym Category Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GymCategory::where('id',$id)->delete();
        return redirect()->back()->with('message', 'Deleted Gym Category Successfully!!!!');
    }
}
