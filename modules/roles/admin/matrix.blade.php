@extends('admin.app')
@section('headSection')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Permissions
                            <small>Create form element</small></h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Permissions</a></li>
                            <li class="breadcrumb-item active">Create From</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->

                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Permissions</h3>
                            </div>
                            <!-- /.card-header -->


                            <form role="form" action="{{ route('admin.roles.matrix.store') }}" method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    @include('includes.messages')

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col"> Permissions </th>
                                                    @if(!empty($roles) && count($roles) > 0)
                                                        @foreach($roles as $role)
                                                            <th scope="col">
                                                                {{ $role->name }}
                                                            </th>
                                                        @endforeach
                                                    @endif
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if(!empty($permissions) && count($permissions))
                                                    @foreach($permissions as $key=>$permission)
                                                        <tr>
                                                            <td scope="row">{{ $permission->name }}</td>
                                                            @if(!empty($roles) && count($roles) > 0)
                                                                @foreach($roles as $role)

                                                                    <td>
                                                                        <div
                                                                            id="rolePermissionsId{{$role->id}}{{$permission->id}}">
                                                                            @foreach($role->permissions as $per)
                                                                                @if($per->id == $permission->id)
                                                                                    <input type="hidden"
                                                                                           name="userHasRole[]"
                                                                                           value="{{$role->id}}">
                                                                                    <input type="hidden"
                                                                                           name="roleHasPermissions[{{$role->id}}][]"
                                                                                           value="{{$permission->id}}">
                                                                                @endif
                                                                            @endforeach
                                                                        </div>

                                                                        <div
                                                                            id="rolePermissionsIdDelete{{$role->id}}{{$permission->id}}">
                                                                        </div>


                                                                        <input type="checkbox"
                                                                               onclick="checkedPermission({{$role->id}},{{$permission->id}})"
                                                                               id="role_{{$role->id}}_{{$permission->id}}"
                                                                               name="role[{{$role->id}}][{{$permission->id}}][]"
                                                                               value=@foreach($role->permissions as $per)@if($per->id == $permission->id) 1
                                                                               @endif @endforeach
                                                                               @foreach($role->permissions as $per)
                                                                               @if($per->id == $permission->id)
                                                                               checked
                                                                            @endif
                                                                            @endforeach
                                                                        >
                                                                    </td>
                                                                @endforeach
                                                            @endif

                                                        </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                            </table>

                                        </div>


                                    </div>


                                    <div class="card-footer">
                                        <input type="submit" class="btn btn-primary">
                                        <a href='{{ route('roles.index') }}' class="btn btn-warning">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->


                    </div>
                    <!--/.col (left) -->

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@endsection


@section('footerSection')
    <!-- jQuery -->
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="{{ asset('admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Bootstrap Switch -->
    <script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        // Summernote
        $('.textarea').summernote();

        function checkedPermission(roleId, permissionsId) {
            var checked = $('#role_' + roleId + '_' + permissionsId);
            if ($(checked).is(':checked') === true) {
                var html = '';
                html += '<input type="hidden" name="userHasRole[]" value=' + roleId + '>';
                html += '<input type="hidden" name="roleHasPermissions[' + roleId + '][]" value=' + permissionsId + '>';
                $('#rolePermissionsId' + roleId + '' + permissionsId).html(html);
                $('#rolePermissionsIdDelete' + roleId + '' + permissionsId).empty();


            } else {
                var deleteHtml = '';
                deleteHtml += '<input type="hidden" name="userHasRoleDelete[]" value=' + roleId + '>';
                deleteHtml += '<input type="hidden" name="roleHasPermissionDelete[' + roleId + '][]" value=' + permissionsId + '>';
                $('#rolePermissionsIdDelete' + roleId + '' + permissionsId).html(deleteHtml);
                $('#rolePermissionsId' + roleId + '' + permissionsId).empty();
            }
        }
    </script>
@endsection

