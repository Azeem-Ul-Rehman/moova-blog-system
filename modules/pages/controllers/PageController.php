<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:page-list|page-create|page-edit|page-delete', ['only' => ['index','store']]);
        $this->middleware('permission:page-create', ['only' => ['create','store']]);
        $this->middleware('permission:page-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:page-delete', ['only' => ['destroy']]);
    }

    //
    public function index()
    {
        $pages = Page::orderBy('created_at', 'DESC')->get();
        return view('admin.page.index', compact('pages'));
    }

    public function create()
    {
        return view('admin.page.create');
    }


    public function store(Request $request)
    {


        $this->validate($request, [
            'page_title' => 'required',
            'page_content' => 'required',
        ]);


        //Create new page
        $page = new Page();
        $page->page_title = $request->page_title;
        $page->page_content = $request->page_content;
        $page->slug = Str::slug($request->slug, '-');
        $page->save();
        return redirect(route('page.index'))->with('message', 'Added Page Successfully!!!!');
    }

    public function edit($id)
    {
        $page = Page::where('id', $id)->first();

        return view('admin.page.edit', compact('page'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'page_title' => 'required',
            'page_content' => 'required',
        ]);

        //Update file
        $page = Page::find($id);
        $page->page_title = $request->page_title;
        $page->page_content = $request->page_content;
        $page->slug = Str::slug($request->slug, '-');
        $page->save();
        return redirect(route('page.index'))->with('message', 'Update Pag Successfully!!!!');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        //Delete image from page

        $page->delete();
        return redirect(route('page.index'))->with('message', 'Deleted Page Successfully!!!!');
    }


}
