<!-- Footer Section -->
@php
    $pages = \App\Models\Page::all();

@endphp
<section class="footer mbl-hide">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="footer-box">
                    <h2>Moova</h2>
                    <span>© 2020 Moova. All Rights Reserved.<br> Operated by TIDAL</span>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="footer-box">
                    <h2>Company</h2>
                    <p>

                        @if(!empty($pages) && count($pages) > 0)
                            @foreach($pages as $index=>$page)
                                <a href="{{ route('user.page.index',[$page->slug])}}" style="    color: #fff;">
                                    {{($page->page_title)}}
                                    @if($index < count($pages) -1)
                                        <br>
                                    @endif
                                </a>

                            @endforeach
                        @endif
                        {{--                        <a href="{{ route('blog.index') }}" style="    color: #fff;">--}}
                        {{--                            Blogs--}}
                        {{--                        </a> <br>--}}
                        {{--                        <a href="{{ route('about.index') }}" style="    color: #fff;">--}}
                        {{--                            About Us--}}
                        {{--                        </a><br>--}}
                        {{--                        <a href="{{ route('contacts.index') }}" style="    color: #fff;">--}}
                        {{--                            Contact Us--}}
                        {{--                        </a><br>--}}
                    </p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="footer-box">
                    <h2>Quick Links</h2>
                    <p>Find a class<br>
                        Moovas Choice<br>
                        Fitness tips<br>
                        Link<br>
                        Link<br>
                        Link</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="footer-box">
                    <h2>Follow us</h2>

                    <img src="{{ asset('assets/images/home/ins.svg') }}">
                    <img src="{{ asset('assets/images/home/twi.svg') }}">
                    <img src="{{ asset('assets/images/home/fb.svg') }}">
                    <img src="{{ asset('assets/images/home/pn.svg') }}">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Mobile Footer Section -->
<section class="mbl-footer desk-hide">
    <div class="container">
        <div id="accordion" class="accordion">
            <div class="footer-card mb-0">
                <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                    <a class="card-title">
                        Company
                    </a>
                </div>
                <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                    @if(!empty($pages) && count($pages) > 0)
                        @foreach($pages as $index=>$page)
                            <a href="{{ route('page.index',[$page->slug])}}" style="    color: #fff;">
                                {{($page->title)}}
                                @if($index < count($pages) -1)
                                    <br>
                                @endif
                            </a>

                        @endforeach
                    @endif
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <a class="card-title">
                        Quick Links
                    </a>
                </div>
                <div id="collapseTwo" class="card-body collapse" data-parent="#accordion">
                    <p>Find a class<br>
                        Moovas Choice<br>
                        Fitness tips<br>
                        Link<br>
                        Link<br>
                        Link
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
            <div class="footer-box">
                <h2>Follow us</h2>
                <img src="{{ asset('assets/images/home/ins.svg') }}">
                <img src="{{ asset('assets/images/home/twi.svg') }}">
                <img src="{{ asset('assets/images/home/fb.svg') }}">
                <img src="{{ asset('assets/images/home/pn.svg') }}">
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
            <div class="footerlast-box">
                <h2>Moova</h2>
                <span>© 2020 Moova. All Rights Reserved. Operated by TIDAL</span>
            </div>
        </div>
    </div>
</section>
