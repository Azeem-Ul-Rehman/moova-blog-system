<section class="menu">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ route('home.index') }}"><img class="header-logo"
                                                                          src="{{ asset('assets/images/home/logo-header.svg') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('how-it-works.index') }}">HOW IT WORKS </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('explore.index') }}">EXPLORE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('pricing.index')}}">FOR PARTNERS</a>
                    </li>


                </ul>

                @if( auth()->check())
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{asset('assets/images/home/account-logo.svg')}}">
                        </a>
                        @if(auth()->user()->is_admin == true)
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">


                                <a class="nav-link" href="{{ route('admin.home') }}">{{ auth()->user()->name }}</a>
                                <div class="dropdown-divider"></div>
                                <a class="nav-link" href="{{ route('logout') }}">Log Out</a>

                            </div>
                        @else
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                <a class="nav-link" href="{{ route('admin.home') }}">{{ auth()->user()->name }}</a>
                                <div class="dropdown-divider"></div>
                                <a class="nav-link" href="{{ route('logout') }}">Log Out</a>

                            </div>
                        @endif

                    </div>



                @endif
                @guest
                    <a class="btn  my-2 my-sm-0 join mr-5" href="{{ route('join-us.index') }}"
                       style="padding-top: 20px;">JOIN NOW</a>
                @endguest


            </div>
        </nav>
    </div>
</section>
