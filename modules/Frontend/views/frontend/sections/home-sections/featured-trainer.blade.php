<!-- FEATURED TRAINERS Section -->

<section class="trainer">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> FEATURED TRAINERS</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem vel
                    tempus. Praesent semper.</p>
            </div>
        </div>

        <div class="row mbl-hide">

            <div id="demo" class="carousel slide" data-ride="carousel">

                <!-- The slideshow -->
                <div class="container carousel-inner no-padding">

                    @if(!empty($trainers) && count($trainers) > 0)
                        @foreach($trainers->chunk(3) as $index=>$trainerObj)
                            <div class="carousel-item {{ $index == 0  ? 'active ': ''}}">
                                @foreach($trainerObj as $index=>$trainer)
                                    <div class="col-xs-3 col-sm-3 col-md-4">
                                        <div class="trainer-card text-center">
                                            <img src="{{asset('storage/images/'.$trainer->image) }}">
                                            <a href="{{ route('trainer-detail.show',[$trainer->id]) }}">
                                                <h2>{{ $trainer->name}}</h2>
                                            </a>
                                            <p>{!! $trainer->about !!}</p>
                                            <div class="trainer-footer">
                                                <img src="{{ asset('assets/images/home/insta.svg') }}">
                                                14k
                                            </div>
                                        </div>
                                    </div>


                                @endforeach
                            </div>
                        @endforeach

                    @endif


                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <div class="icon-box">
                        <span class="carousel-control"> <img
                                src="{{ asset('assets/images/home/icons8-arrow-pointing-left-24.png') }}"></span>
                    </div>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <div class="icon-boxtwo">
                        <span class="carousel-control"><img src="{{ asset('assets/images/home/icons8-right-30.png') }}"></span>
                    </div>
                </a>
            </div>

        </div>


        <!--Featured Trainer Mobile carousel -->

        <div class="carousel-wrap desk-hide">
            <div class="owl-carousel">
                @if(!empty($trainers) && count($trainers) > 0)
                    @foreach($trainers as $trainer)
                        <div class="item">
                            <div class="trainer-card text-center">
                                <img class="trainer-profile" src="{{asset('storage/images/'.$trainer->image) }}">

                                <h2>{{ $trainer->name}}</h2>

                                <p>{!! $trainer->about !!}</p>
                                <div class="trainer-footer">
                                    <img src="{{ asset('assets/images/home/insta.svg') }}"> 14k
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

        </div>


    </div>
</section>
