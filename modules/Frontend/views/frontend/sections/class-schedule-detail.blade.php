@if(!empty($classes) && count($classes) > 0)
    @foreach($classes as $class)
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <p class="result-date">{{ date('l, F d ',strtotime($class->start_date) ) }}</p>
            </div>
        </div>

        <div class="row">

            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                @if(!empty($class->classScheduleTime) && count($class->classScheduleTime) > 0)
                    @foreach($class->classScheduleTime as $time)
                        <p>{{ date('h:i A',strtotime($time->start_time)) }} <br>EDT<br>
                            {{ $time->duration }} min</p>
                        <br>
                    @endforeach
                @endif
            </div>
            @if($type == 'gym')
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                    <h5>{{ $class->uClass->name }}</h5>
                    <p>{{ $class->instructor->name }}</p>
                </div>
            @else
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                    <h5>{{ $class->uClass->name }}</h5>
                    <p>{{ $class->instructor->name }}</p>
                </div>
            @endif

            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                <button type="button" class="btn join-btn">Join</button>
            </div>


        </div>

        <hr>
    @endforeach
@endif



