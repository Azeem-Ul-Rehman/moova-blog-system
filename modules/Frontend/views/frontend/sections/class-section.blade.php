@if(!empty($classes) && count($classes) > 0)

    <h2 class=" pt-5">Classes</h2>

    <div class="row classes ">

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="dropdown show">
                <a class="btn class-btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Classes
                </a>

                <div class="dropdown-menu class-menu" aria-labelledby="dropdownMenuLink">

                    @if(!empty($classes) && count($classes) > 0)
                        @foreach($classes as $class)

                            @php
                                $uClass = \App\Models\UClass::where('id',$class->class_id)->first();

                                if($type == 'gym'){
                                    $request_id = $gym->id;
                                }else{
                                    $request_id = $instructor->id;
                                }
                            @endphp
                            <a class="dropdown-item" href="javascript:void(0)"
                               onclick="classDetail('{{$uClass->id}}','{{$type}}','{{$request_id}}')">{{ $uClass->name }}</a>
                        @endforeach


                    @endif
                </div>
            </div>

        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
            <div class="dropdown show">
                <a class="btn dates-btn " href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Available Dates
                </a>

                <div class="dropdown-menu dates-menu" aria-labelledby="dropdownMenuLink">
                    @if(!empty($availableDates) && count($availableDates) > 0)
                        @foreach($availableDates as $dates)
                            @php
                                if($type == 'gym'){
                                    $request_id = $gym->id;
                                }else{
                                    $request_id = $instructor->id;
                                }
                            @endphp
                            <a class="dropdown-item"
                               href="javascript:void(0)"
                               onclick="classDetailDate('{{$type}}','{{$request_id}}','{{$dates->start_date}}')">{{ date('l, F d ',strtotime($dates->start_date) ) }}</a>
                        @endforeach


                    @endif
                </div>
            </div>

        </div>

    </div>

    <section class="result-class">


    </section>
@endif

@push('js')
    <script>


        function classDetail(class_id, type, request_id) {
            var token = $("input[name='_token']").val();
            $.ajax({
                url: "{{ route('class-schedule.detail') }}",
                method: 'POST',
                data: {class_id: class_id, type: type, id: request_id, _token: token},
                success: function (data) {
                    $(".result-class").html('');
                    $(".result-class").html(data.options);
                }
            });
        }

        function classDetailDate(type, request_id, date) {
            var token = $("input[name='_token']").val();
            $.ajax({
                url: "{{ route('class-schedule.detail.date') }}",
                method: 'POST',
                data: {type: type, id: request_id, date: date, _token: token},
                success: function (data) {
                    $(".result-class").html('');
                    $(".result-class").html(data.options);
                }
            });
        }


    </script>
@endpush
