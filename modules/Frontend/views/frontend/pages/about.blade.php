@extends('frontend.layouts.app')
@section('content')
    <!-- About Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> {{ ucwords($page->page_title) }}</h2>
                    {{--                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem--}}
                    {{--                        vel--}}
                    {{--                        tempus. Praesent semper.</p>--}}
                </div>
            </div>
        </div>
    </section>

    <!-- About Detail Section -->

    <section class="about-detail">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                    <div class="about-info">
                        {!!  $page->page_content !!}
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection

