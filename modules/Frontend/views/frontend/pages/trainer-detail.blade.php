@extends('frontend.layouts.app')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
@section('content')

    <!-- Trainer page-header -->
    <div class="trainer-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @include('includes.messages')
                </div>
            </div>
        </div>
    </div>
    <!-- /.Trainer page-header-->


    <!-- news -->
    <div class="card-section">
        <div class="container text-center">
            <div class="card-trainer bg-white mb30">
                <div class="row">

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- section-title -->
                        <div class="text-center mb-0">
                            <img src="{{asset('storage/images/'.$instructor->image) }}">
                            <h2 class="trainer-title">{{ $instructor->name}}</h2>
                            <p class="trainer-dec">
                                <img style="margin-top: -5px;"
                                     src="{{ asset('assets/images/trainer-detail/F176A1B7-BA70-4FD3-B0E1-88E6CF2842D2.svg') }}">
                                0 Following • 467 Followers</p>
                        </div>
                        <!-- /.section-title -->
                        <!-- section-star -->
                        <div class="ratig text-center">
                            @if($reviewStars > 0 && $reviewStars <=1)
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 1 && $reviewStars <=2)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 2 && $reviewStars <=3)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 3 && $reviewStars <=4)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 4 && $reviewStars <=5)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewCount > 0)
                                <span class="reviews ml-2 mt-2">{{$reviewCount}} reviews</span>
                            @endif
                        </div>
                        <!-- /.section-start -->
                        <button class="btn   join" type="submit">BOOK CLASS</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /.news -->


    <div class="discription-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                    <h2>Description</h2>
                    <p>{!! $instructor->description !!}</p>

                    <h2 class=" mt-5">Gallery</h2>

                    <div class="row mt-4">
                        <img class="gallery-img" src="{{ asset('assets/images/trainer-detail/glry-1.jpg') }}">
                        <img class="gallery-img ml-3" src="{{ asset('assets/images/trainer-detail/glry-2.jpg') }}">
                        <img class="gallery-img ml-3" src="{{ asset('assets/images/trainer-detail/glry-3.jpg') }}">
                        <img class="gallery-img ml-3" src="{{ asset('assets/images/trainer-detail/glry-4.jpg') }}">
                    </div>
                   


                    @include('frontend.sections.class-section')




                    <div class="revw-sec">

                        @if(!empty($reviews))
                            @foreach($reviews as $key=>$review)
                                <div class="row @if($key == 0 ) mt-5 @endif">
                                    @if($key == 0)
                                        <h2>Reviews</h2>
                                    @endif

                                </div>
                                <div class="row">
                                    <p>{{ $review->description }} </p>

                                </div>
                                <div class="row">
                                    <p>{{ $review->title }} </p>
                                    <p>
                                        @if($review->rating > 0 && $review->rating <=1)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 1 && $review->rating <=2)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 2 && $review->rating <=3)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 3 && $review->rating <=4)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 4 && $review->rating <=5)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                    </p>
                                </div>
                                <div class="row">
                                    <p>{{ date('d-m-Y', strtotime($review->created_at)) }}</p>
                                </div>
                                <hr class="hr-review">
                            @endforeach
                        @endif

                    </div>


                    <!-- Button trigger modal -->
                    <a type="button" class="btn rev-btn mt-5"  data-toggle="modal" data-target="#exampleModal">
                        Give Review
                        </a>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Review</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('review.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">


                                        <input type="hidden" name="user_id"
                                               value="{{ auth()->check() ? auth()->user()->id : '' }}">
                                        <input type="hidden" name="type_id" value="{{ $instructor->id}}">
                                        <input type="hidden" name="type" value="instructor">

                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control"
                                                   id="title">
                                        </div>


                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea class="form-control" id="description"
                                                      name="description" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="rating">Rating</label>

                                            <br>
                                            <div class="rate">
                                                <input type="radio" id="star5" name="rating" value="5"/>
                                                <label for="star5" title="text">5 stars</label>
                                                <input type="radio" id="star4" name="rating" value="4"/>
                                                <label for="star4" title="text">4 stars</label>
                                                <input type="radio" id="star3" name="rating" value="3"/>
                                                <label for="star3" title="text">3 stars</label>
                                                <input type="radio" id="star2" name="rating" value="2"/>
                                                <label for="star2" title="text">2 stars</label>
                                                <input type="radio" id="star1" name="rating" value="1"/>
                                                <label for="star1" title="text">1 star</label>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="clearfix"></div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Add
                                            Review
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- /col-8 --> </div>


                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                    <div class="card-more mt-5">
                        <h2>
                            More information
                        </h2>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-6">
                                <P class="dark">Languages<br>
                                    Area<br>
                                    Level<br>
                                    Tags<br>
                                </P>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6">
                                <P>English<br>
                                    Dubai Marina<br>
                                    All levels<br>
                                    Asana<br>
                                </P>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- /row -->
            </div>
        </div>
    </div>



@endsection
<style>
    * {
        margin: 0;
        padding: 0;
    }

    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }

    .rate:not(:checked) > input {
        position: absolute;
        top: -9999px;
    }

    .rate:not(:checked) > label {
        float: right;
        width: 1em;
        overflow: hidden;
        white-space: nowrap;
        cursor: pointer;
        font-size: 30px;
        color: #ccc;
    }

    .rate:not(:checked) > label:before {
        content: '★ ';
    }

    .rate > input:checked ~ label {
        color: #ffc700;
    }

    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;
    }

    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }

    /* Modified from: https://github.com/mukulkant/Star-rating-using-pure-css */
</style>


