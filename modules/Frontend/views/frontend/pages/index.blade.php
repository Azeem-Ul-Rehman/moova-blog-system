<?php include 'partial/header.php';?>



<!-- Page Header Section -->

<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>Instantly book gym classes nearby</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <button class="btn">DISCOVER GYMS NEAR ME</button>
            </div>
        </div>
    </div>
</section>

<!-- Category Section -->

<?php include 'sections/home-sections/category-section.php';?>

<!-- Moova Choice Section -->

<?php include 'sections/home-sections/moova-choice.php';?>



<!-- Heading SEO Section -->

<section class="heading-seo">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="seo-box">
                    <h2>Heading for SEO</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 bg-img">
                <img src="assets/images/home/women.png">
            </div>
        </div>
    </div>

</section>

<!-- Works Section -->

<section class="works">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="assets/images/home/section-logo.svg"> SEE HOW IT WORKS</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="assets/images/home/train.svg"></div>
                    <h2>Choose What To Train</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="assets/images/home/like.svg"></div>
                    <h2>Find What You Like</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="assets/images/home/amazing.svg"></div>
                    <h2>Explore Amazing Gyms</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

        </div>

    </div>

</section>

<!-- Fitness Section -->

<?php include 'sections/home-sections/fitness-tips.php';?>

<!-- featured trainer Section -->

<?php include 'sections/home-sections/featured-trainer.php';?>


<!-- REVIEWS Section -->

<section class="review">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="assets/images/home/section-logo.svg"> REVIEWS</h2>
            </div>
        </div>
        <div class="row ">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="review-card text-center">
                    <img src="assets/images/home/client1.png">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam egestas ipsum sed urna sodales
                        lacinia. Morbi.</p>
                    <h2>Full name</h2>
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="review-card text-center">
                    <img src="assets/images/home/client2.png">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam egestas ipsum sed urna sodales
                        lacinia. Morbi.</p>
                    <h2>Full name</h2>
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                    <img class="star" src="assets/images/home/star.svg">
                </div>
            </div>

        </div>
    </div>
</section>

<?php include 'partial/footer.php';?>

