<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <script src="https://kit.fontawesome.com/e511cdbaf6.js" crossorigin="anonymous"></script>

    <title>Moova</title>
</head>
<body class="antialiased">
@include('frontend.partial.header')


<!-- Page Header Section -->
@include('includes.messages')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>Instantly book gym classes nearby</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <button class="btn">DISCOVER GYMS NEAR ME</button>
            </div>
        </div>
    </div>
</section>

<!-- Category Section -->

@include('frontend.sections.home-sections.category-section')


<!-- Moova Choice Section -->

@include('frontend.sections.home-sections.moova-choice')


<!-- Heading SEO Section -->

<section class="heading-seo">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="seo-box">
                    <h2>Heading for SEO</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 bg-img">
                <img src="{{asset('assets/images/home/women.png')}}">
            </div>
        </div>
    </div>

</section>

<!-- Works Section -->

<section class="works">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> SEE HOW IT WORKS</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="{{ asset('assets/images/home/train.svg') }}"></div>
                    <h2>Choose What To Train</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="{{ asset('assets/images/home/like.svg') }}"></div>
                    <h2>Find What You Like</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 text-center">
                <div class="work-box">
                    <div class="work-img">
                        <img src="{{ asset('assets/images/home/amazing.svg') }}"></div>
                    <h2>Explore Amazing Gyms</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>

        </div>

    </div>

</section>

<!-- Fitness Section -->
@include('frontend.sections.home-sections.fitness-tips')

<!-- featured trainer Section -->

@include('frontend.sections.home-sections.featured-trainer')


<!-- REVIEWS Section -->

<section class="review">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> REVIEWS</h2>
            </div>
        </div>
        <div class="row ">
            @if(!empty($reviews) && count($reviews) > 0)
                @foreach($reviews as $review)
                    @php
                    if($review->type == 'gym'){
                        $gym = \App\Models\Gym::where('id',$review->type_id)->first();

                         if(!is_null($gym)){
                            $image = 'storage/images/'.$gym->image;
                            $name = $gym->name;
                        }else{
                             $image = 'assets/images/home/client1.png';
                             $name = 'Name Not Found';
                        }
                    }else{
                        $instructor = \App\Models\Instructor::where('id',$review->type_id)->first();
                        if(!is_null($instructor)){
                            $image = 'storage/images/'.$instructor->image;
                            $name = $instructor->name;
                        }else{
                             $image = 'assets/images/home/client1.png';
                             $name = 'Name Not Found';
                        }

                    }

                    @endphp

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    
                          
                       
                        <div class="review-card text-center">
                            <img src="{{ asset(''.$image.'') }}">
                            <p>
                            {!! \Illuminate\Support\str::limit(strip_tags($review->description), $limit = 100, $end = '...') !!}
                            </p>
                            <h2>{{ucwords($name)}}</h2>
                            @if($review->rating > 0 && $review->rating <=1)
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                            @endif
                            @if($review->rating > 1 && $review->rating <=2)
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                            @endif
                            @if($review->rating > 2 && $review->rating <=3)
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                            @endif
                            @if($review->rating > 3 && $review->rating <=4)
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                            @endif
                            @if($review->rating > 4 && $review->rating <=5)
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                            @endif


                        </div>
                    </div>
                    
                @endforeach
            @endif
        </div>
    </div>
</section>


@include('frontend.partial.footer')

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"-->
<!--        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"-->
<!--        crossorigin="anonymous"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>

<script src="{{asset('assets/css/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>
