@extends('frontend.layouts.app')
@section('content')

    <!-- join us Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> SIGN IN</h2>
                    @if (count($errors) > 0)
                    @else
                        @include('includes.messages')
                    @endif
                </div>
            </div>
        </div>
    </section>


    <!-- join us Card Section -->

    <section class="join-card">
        <div class="container">
            <div class="row">
                <div class="join-body">
                    <h2 class="text-center">Log In</h2>

                    <form method="POST" action="{{ route('login.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                   name="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password" name="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group text-center">
                            <button style="cursor:pointer" type="submit" class="btn join">Login</button>
                            <a class="btn btn-link" href="/forget-password">
                                Forgot Your Password?
                            </a>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </section>


    <!-- Footer Section -->

@endsection
