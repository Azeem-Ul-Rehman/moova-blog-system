@extends('frontend.layouts.app')
@section('content')

    <!-- join us Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> Forgot Password</h2>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>


    <!-- join us Card Section -->

    <section class="join-card">
        <div class="container">
            <div class="row">
                <div class="join-body login-body" style="height: auto !important;">
                    <h2 class="text-center">Reset Password</h2>

                    <form method="POST" action="/reset-password" >
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password"
                                   autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password-confirm">Confirm Password:</label>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" autocomplete="new-password">

                        </div>

                        <div class="form-group text-center">
                            <button style="cursor:pointer;width: 100%" type="submit" class="btn join">  Reset Password
                            </button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </section>


    <!-- Footer Section -->

@endsection
