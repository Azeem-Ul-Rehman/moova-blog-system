@extends('frontend.layouts.app')
@section('content')

    <!-- join us Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> JOIN US</h2>
{{--                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem--}}
{{--                        vel--}}
{{--                        tempus. Praesent semper.</p>--}}
                </div>
            </div>
        </div>
    </section>


    <!-- join us Card Section -->

    <section class="join-card">
        <div class="container">
            <div class="row">
                <div class="join-body">
                <h2 class="text-center">REGISTER</h2>
                    <form method="POST" action="{{ route('register.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                   name="name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   id="email" name="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password" name="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group text-center">
                            <button style="cursor:pointer" type="submit" class="btn join">Submit</button>
                        </div>
                        {{--                        @include('partials.formerrors')--}}
                    </form>
                </div>


            </div>
        </div>
    </section>


    <!-- Footer Section -->

@endsection
