<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function create()
    {
        if (auth()->check()) {
            return redirect()->route('home.index')->with('message', 'Account Already Login.');
        } else {
            return view('auth.login');
        }

    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);
        if (auth()->attempt($credentials)) {


            if (auth()->user()->status == 'verified') {
                if (auth()->user()->is_admin) {
                    return redirect()->route('admin.home');
                }

                return redirect()->route('home.index');
            } else {

                Auth::logout();
                return redirect()->back()->with('danger', 'Your Account is suspended.');
            }


        } else {
            Auth::logout();
            return redirect()->back()->with('danger', 'Invalid credentials');
        }

    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('home.index');
    }
}
