<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;

class BlogController extends Controller
{
    //
    public function index()
    {
        $posts = Post::with(['categories', 'tags'])->paginate(9);
        $featuredPost = Post::where('is_featured',true)->orderBy('id','DESC')->with(['categories', 'tags'])->first();
        return view('frontend.pages.blog.blogs', compact('posts','featuredPost'));
    }

    public function show($id)
    {
        $post = Post::find($id);
        $tags = Tag::all();
        $categories = Category::where('type', 'post')->get();
        return view('frontend.pages.blog.single-blog', compact('post', 'tags', 'categories'));
        //return view('frontend.pages.blog.single-blog')->with('post',$posts);
    }
    public function category($id)
    {
        $posts = Post::whereHas('categories', function ($q) use ($id) {
            $q->where('id', $id);
        })->paginate(5);
        $tags = Tag::all();
        $categories = Category::where('type', 'post')->get();
        $featuredPost = Post::where('is_featured',true)->orderBy('id','DESC')->whereHas('categories', function ($q) use ($id) {
            $q->where('id', $id);
        })->first();
        

        return view('frontend.pages.blog.blogs', compact('featuredPost','posts', 'tags', 'categories', 'id'));
    }


    public function tags($id)
    {
        $posts = Post::whereHas('tags', function ($q) use ($id) {
            $q->where('id', $id);
        })->paginate(9);
        $tags = Tag::all();
        $categories = Category::where('type', 'post')->get();
        $featuredPost = Post::where('is_featured',true)->orderBy('id','DESC')->whereHas('tags', function ($q) use ($id) {
            $q->where('id', $id);
        })->first();
        

        return view('frontend.pages.blog.blogs', compact('featuredPost','posts', 'tags', 'categories', 'id'));
    }

}
