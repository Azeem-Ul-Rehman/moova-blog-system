<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'type_id' => 'required',
            'type' => 'required',
            'rating' => 'required',
            'title' => 'required',
            'description' => 'required',
        ],[
            'user_id.required' =>'User must be login.',
            'type_id.required' =>'Type is required.',
            'rating.required' =>'Rating is required.',
            'title.required' =>'Title is required.',
            'description.required' =>'Description is required.',
        ]);
        $review = new Review();
        $review->user_id = $request->user_id;
        $review->type_id = $request->type_id;
        $review->type = $request->type;
        $review->rating = $request->rating;
        $review->title = $request->title;
        $review->description = $request->description;
        $review->status = 'unapproved';
        $review->save();
        return redirect()->back()->with('message', 'Review Added Successfully!!!!');
    }
}
