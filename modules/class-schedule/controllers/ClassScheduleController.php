<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\ClassSchedule;
use App\Models\ClassScheduleTime;
use App\Models\ClassSingleDayException;
use App\Models\Gym;
use App\Models\Instructor;
use App\Models\UClass;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClassScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:class-schedule-list|class-schedule-create|class-schedule-edit|class-schedule-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:class-schedule-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:class-schedule-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:class-schedule-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classSchedules = ClassSchedule::orderBy('created_at', 'DESC')->with(['uClass', 'instructor'])->get();
        return view('admin.class-schedule.index', compact('classSchedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = UClass::all();
        $instructors = Instructor::all();
        $gyms = Gym::all();
        return view('admin.class-schedule.create', compact('classes', 'instructors', 'gyms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'class_type' => 'required',
            'class_id' => 'required',
            'instructor_id' => 'required',
            'gym_id' => 'required',
            'start_date' => 'required',
            'end_date' => Rule::requiredIf(function () use ($request) {
                if ($request->class_type == 'recurring' && $request->no_end_date == 0) {
                    return true;
                }
            }),
            'no_end_date' => Rule::requiredIf(function () use ($request) {
                if ($request->no_end_date == 1) {
                    return true;
                }
            }),
            'cp_spots' => 'required',
            'capacity' => 'required',
        ]);

        $class_schedule = new ClassSchedule();
        $class_schedule->class_type = $request->class_type;
        $class_schedule->class_id = $request->class_id;
        $class_schedule->instructor_id = $request->instructor_id;
        $class_schedule->gym_id = $request->gym_id;
        $class_schedule->start_date = $request->start_date;
        $class_schedule->end_date = $request->end_date;
        $class_schedule->no_end_date = isset($request->no_end_date) ? $request->no_end_date : 0;
        $class_schedule->cp_spots = $request->cp_spots;
        $class_schedule->capacity = $request->capacity;
        $class_schedule->monday = isset($request->monday) ? $request->monday : 0;
        $class_schedule->tuesday = isset($request->tuesday) ? $request->tuesday : 0;
        $class_schedule->wednesday = isset($request->wednesday) ? $request->wednesday : 0;
        $class_schedule->thursday = isset($request->thursday) ? $request->thursday : 0;
        $class_schedule->friday = isset($request->friday) ? $request->friday : 0;
        $class_schedule->saturaday = isset($request->saturaday) ? $request->saturaday : 0;
        $class_schedule->sunday = isset($request->sunday) ? $request->sunday : 0;
        $class_schedule->save();

        $a = 0;
        if (!empty($request->start_time) && count($request->start_time) > 0) {
            foreach ($request->start_time as $time) {
                $startTime = $time;
                ClassScheduleTime::create([
                    'class_schedule_id' => $class_schedule->id,
                    'start_time' => date("H:i", strtotime($startTime)),
                    'duration' => $request->duration[$a],
                ]);
                $a++;

            }
        }


        $b = 0;
        if (!empty($request->date) && count($request->date) > 0) {
            foreach ($request->date as $date) {
                ClassSingleDayException::create([
                    'class_schedule_id' => $class_schedule->id,
                    'date' => $date,
                ]);
                $b++;

            }
        }
        return redirect(route('class-schedule.index'))->with('message', 'Class Schedule Added Successfully!!!!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $instructors = Instructor::all();
        $gyms = Gym::all();
        $classes = UClass::all();
        $classSchedule = ClassSchedule::where('id',$id)->with(['uClass','instructor','gym','classScheduleTime','classScheduleSingleDay'])->first();
        return view('admin.class-schedule.edit',compact('classes', 'instructors', 'gyms','classSchedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_type' => 'required',
            'class_id' => 'required',
            'instructor_id' => 'required',
            'gym_id' => 'required',
            'start_date' => 'required',
            'end_date' => Rule::requiredIf(function () use ($request) {
                if ($request->class_type == 'recurring' && $request->no_end_date == 0) {
                    return true;
                }
            }),
            'no_end_date' => Rule::requiredIf(function () use ($request) {
                if ($request->no_end_date == 1) {
                    return true;
                }
            }),
            'cp_spots' => 'required',
            'capacity' => 'required',
        ]);

        $class_schedule = ClassSchedule::where('id',$id)->first();
        $class_schedule->class_type = $request->class_type;
        $class_schedule->class_id = $request->class_id;
        $class_schedule->instructor_id = $request->instructor_id;
        $class_schedule->gym_id = $request->gym_id;
        $class_schedule->start_date = $request->start_date;
        $class_schedule->end_date = $request->end_date;
        $class_schedule->no_end_date = isset($request->no_end_date) ? $request->no_end_date : 0;
        $class_schedule->cp_spots = $request->cp_spots;
        $class_schedule->capacity = $request->capacity;
        $class_schedule->monday = isset($request->monday) ? $request->monday : 0;
        $class_schedule->tuesday = isset($request->tuesday) ? $request->tuesday : 0;
        $class_schedule->wednesday = isset($request->wednesday) ? $request->wednesday : 0;
        $class_schedule->thursday = isset($request->thursday) ? $request->thursday : 0;
        $class_schedule->friday = isset($request->friday) ? $request->friday : 0;
        $class_schedule->saturaday = isset($request->saturaday) ? $request->saturaday : 0;
        $class_schedule->sunday = isset($request->sunday) ? $request->sunday : 0;
        $class_schedule->save();


        $classScheduleTime =   ClassScheduleTime::where('class_schedule_id',$class_schedule->id)->get();
        if(!empty($classScheduleTime) && count($classScheduleTime) > 0){
            ClassScheduleTime::where('class_schedule_id',$class_schedule->id)->delete();
        }
        $a = 0;
        if (!empty($request->start_time) && count($request->start_time) > 0) {
            foreach ($request->start_time as $time) {
                $startTime = $time;
                ClassScheduleTime::create([
                    'class_schedule_id' => $class_schedule->id,
                    'start_time' => date("H:i", strtotime($startTime)),
                    'duration' => $request->duration[$a],
                ]);
                $a++;

            }
        }


        $classSingleDayException =   ClassSingleDayException::where('class_schedule_id',$class_schedule->id)->get();
        if(!empty($classSingleDayException) && count($classSingleDayException) > 0){
            ClassSingleDayException::where('class_schedule_id',$class_schedule->id)->delete();
        }

        $b = 0;
        if (!empty($request->date) && count($request->date) > 0) {
            foreach ($request->date as $date) {
                ClassSingleDayException::create([
                    'class_schedule_id' => $class_schedule->id,
                    'date' => $date,
                ]);
                $b++;

            }
        }

        return redirect(route('class-schedule.index'))->with('message', 'Class Schedule Updated Successfully!!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classSchedule = ClassSchedule::where('id', $id)->first();
        if (!empty($classSchedule->classScheduleTime) && count($classSchedule->classScheduleTime) > 0) {
            $classSchedule->classScheduleTime()->delete();
        }
        if (!empty($classSchedule->classScheduleSingleDay) && count($classSchedule->classScheduleSingleDay) > 0) {
            $classSchedule->classScheduleSingleDay()->delete();
        }

        $classSchedule->delete();
        return redirect()->back()->with('message', 'Deleted Class Schedule Successfully!!!!');
    }


}
