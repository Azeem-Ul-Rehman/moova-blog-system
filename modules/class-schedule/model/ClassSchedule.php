<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    use HasFactory;

    public function uClass()
    {
        return $this->belongsTo(UClass::class, 'class_id', 'id');
    }

    public function instructor()
    {
        return $this->belongsTo(Instructor::class, 'instructor_id', 'id');
    }

    public function gym()
    {
        return $this->belongsTo(Gym::class, 'gym_id', 'id');
    }

    public function classScheduleTime()
    {
        return $this->hasMany(ClassScheduleTime::class, 'class_schedule_id', 'id');
    }

    public function classScheduleSingleDay()
    {
        return $this->hasMany(ClassSingleDayException::class, 'class_schedule_id', 'id');
    }
}
