@extends('admin.app')
@section('headSection')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Class
                            <small>Create form element</small></h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('class-schedule.index') }}">Class Table</a>
                            </li>
                            <li class="breadcrumb-item active">Create From</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Class Schedule</h3>
                            </div>
                            <!-- /.card-header -->


                            <form role="form" action="{{ route('class-schedule.store') }}" method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    @include('includes.messages')

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Class Type</label>
                                                <input type="radio" class="form-control" id="class_type"
                                                       name="class_type" value="recurring"
                                                       placeholder="Class Type">Recurring
                                                <input type="radio" class="form-control" id="class_type"
                                                       name="class_type" value="one_time"
                                                       placeholder="Class Type">One Time
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="class_id">Select Class</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Class" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="class_id" id="class_id">
                                                    @if(!empty($classes) && count($classes) >0)
                                                        @foreach ($classes as $class)
                                                            <option
                                                                value="{{ $class->id }}">{{ $class->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="start_date">Start Date</label>
                                                <input type="date" class="form-control" id="start_date"
                                                       name="start_date"
                                                       placeholder="Start Date">
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="end_date_recurring" style="display: none">
                                            <div class="form-group">
                                                <div id="no_end_Date_checked">
                                                    <label for="end_date">End Date</label>
                                                    <input type="date" class="form-control" id="end_date"
                                                           name="end_date"
                                                           placeholder="End Date">
                                                </div>


                                                <input type="checkbox" class="form-control" id="no_end_date"
                                                       name="no_end_date" value="0"
                                                       placeholder="No End Date">No End Date
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="instructor_id">Select Instructor</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Instructor" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="instructor_id" id="instructor_id">
                                                    @if(!empty($instructors) && count($instructors) >0)
                                                        @foreach ($instructors as $instructor)
                                                            <option
                                                                value="{{ $instructor->id }}">{{ $instructor->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="gym_id">Select Gym</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Gym" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="gym_id" id="gym_id">
                                                    @if(!empty($gyms) && count($gyms) >0)
                                                        @foreach ($gyms as $gym)
                                                            <option
                                                                value="{{ $gym->id }}">{{ $gym->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-card">
                                                <div class="form-group">
                                                    <div id="all_contacts">

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cp_spots">Cp Spots</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Cp Spots" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="cp_spots" id="cp_spots">

                                                    <option value="01" selected>1</option>
                                                    <option value="02">2</option>
                                                    <option value="03">3</option>
                                                    <option value="04">4</option>
                                                    <option value="05">5</option>
                                                    <option value="06">6</option>
                                                    <option value="07">7</option>
                                                    <option value="08">8</option>
                                                    <option value="09">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>
                                                    <option value="24">24</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="capacity">Capacity</label>
                                                <select class="form-control"
                                                        data-placeholder="Select Capacity" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="capacity" id="capacity">

                                                    <option value="01" selected>1</option>
                                                    <option value="02">2</option>
                                                    <option value="03">3</option>
                                                    <option value="04">4</option>
                                                    <option value="05">5</option>
                                                    <option value="06">6</option>
                                                    <option value="07">7</option>
                                                    <option value="08">8</option>
                                                    <option value="09">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                    <option value="13">13</option>
                                                    <option value="14">14</option>
                                                    <option value="15">15</option>
                                                    <option value="16">16</option>
                                                    <option value="17">17</option>
                                                    <option value="18">18</option>
                                                    <option value="19">19</option>
                                                    <option value="20">20</option>
                                                    <option value="21">21</option>
                                                    <option value="22">22</option>
                                                    <option value="23">23</option>
                                                    <option value="24">24</option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="row" id="repeat_on_recurring" style="display: none">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Repeat On</label>
                                                <br>
                                                <input type="checkbox" name="monday" id="monday" value="0">Monday<br>
                                                <input type="checkbox" name="tuesday" id="tuesday" value="0">Tuesday<br>
                                                <input type="checkbox" name="wednesday" id="wednesday" value="0">Wednesday<br>
                                                <input type="checkbox" name="thursday" id="thursday"
                                                       value="0">Thursday<br>
                                                <input type="checkbox" name="friday" id="friday" value="0">Friday<br>
                                                <input type="checkbox" name="saturaday" id="saturaday" value="0">Saturday<br>
                                                <input type="checkbox" name="sunday" id="sunday" value="0">Sunday<br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="single_date_recurring" style="display: none">
                                        <div class="col-md-6">
                                            <div class="form-card">
                                                <div class="form-group">
                                                    <div id="all_contacts_time">

                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="card-footer">
                                    <input type="submit" class="btn btn-primary">
                                    <a href='{{ route('class-schedule.index') }}' class="btn btn-warning">Back</a>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->


                    </div>
                    <!--/.col (left) -->

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


@section('footerSection')
    <!-- jQuery -->
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="{{ asset('admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Bootstrap Switch -->
    <script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        // Summernote
        $('.textarea').summernote()

        $('#no_end_date').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
                $('#no_end_Date_checked').css('display', 'none');
            } else {
                $(this).val(0);
                $('#no_end_Date_checked').css('display', '');
            }
        });

        $("input[name='class_type']").change(function () {
            debugger;
            if ($(this).val() === 'recurring') {
                $('#end_date_recurring').css('display', '');
                $('#repeat_on_recurring').css('display', '');
                $('#single_date_recurring').css('display', '');
            } else {
                $('#end_date_recurring').css('display', 'none');
                $('#repeat_on_recurring').css('display', 'none');
                $('#single_date_recurring').css('display', 'none');
            }

        });
        $(document).ready(function () {
            var count = 1;

            dynamic_field(count);

            function dynamic_field(number) {
                html = '<div id="emergency_contacts' + number + '">';
                html += '<select class="form-control" id="start_time" name="start_time[]" >\n' +
                    '\n' +
                    '                                                            <option value="01:00" selected>01:00</option>\n' +
                    '                                                            <option value="02:00">02:00</option>\n' +
                    '                                                            <option value="03:00">03:00</option>\n' +
                    '                                                            <option value="04:00">04:00</option>\n' +
                    '                                                            <option value="05:00">05:00</option>\n' +
                    '                                                            <option value="06:00">06:00</option>\n' +
                    '                                                            <option value="07:00">07:00</option>\n' +
                    '                                                            <option value="08:00">08:00</option>\n' +
                    '                                                            <option value="09:00">09:00</option>\n' +
                    '                                                            <option value="10:00">10:00</option>\n' +
                    '                                                            <option value="11:00">11:00</option>\n' +
                    '                                                            <option value="12:00">12:00</option>\n' +
                    '                                                            <option value="13:00">13:00</option>\n' +
                    '                                                            <option value="14:00">14:00</option>\n' +
                    '                                                            <option value="15:00">15:00</option>\n' +
                    '                                                            <option value="16:00">16:00</option>\n' +
                    '                                                            <option value="17:00">17:00</option>\n' +
                    '                                                            <option value="18:00">18:00</option>\n' +
                    '                                                            <option value="19:00">19:00</option>\n' +
                    '                                                            <option value="20:00">20:00</option>\n' +
                    '                                                            <option value="21:00">21:00</option>\n' +
                    '                                                            <option value="22:00">22:00</option>\n' +
                    '                                                            <option value="23:00">23:00</option>\n' +
                    '                                                            <option value="24:00">24:00</option>\n' +
                    '                                                        </select>';
                html += '<input type="number" name="duration[]" placeholder="Duration" />';
                if (number > 1) {
                    html += '<button type="button" name="remove" id="" class="btn btn-danger remove">X</button></div>';
                    $('#all_contacts').append(html);
                } else {
                    html += '<button type="button" name="add" id="add" class="btn btn-success">ADD More Start Times</button></div>';
                    $('#all_contacts').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);
            });

            $(document).on('click', '.remove', function () {
                var removecount = count;
                count--;
                $('#emergency_contacts' + removecount + '').remove();
            });





        //    Single Day Exception

            var count1 = 1;

            dynamic_field1(count1);

            function dynamic_field1(number) {
                html = '<div id="emergency_contacts_time' + number + '">';
                html += '<input type="date" name="date[]" placeholder="dd/mm/yyyy" />';
                if (number > 1) {
                    html += '<button type="button" name="remove1" id="" class="btn btn-danger remove1">X</button></div>';
                    $('#all_contacts_time').append(html);
                } else {
                    html += '<button type="button" name="add1" id="add1" class="btn btn-success">ADD More Single Day Exception</button></div>';
                    $('#all_contacts_time').html(html);
                }
            }

            $(document).on('click', '#add1', function () {
                count1++;
                dynamic_field1(count1);
            });

            $(document).on('click', '.remove1', function () {
                var removecount1 = count1;
                count1--;
                $('#emergency_contacts_time' + removecount1 + '').remove();
            });
        });


        $('#monday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#tuesday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#wednesday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#thursday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#friday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#saturaday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#sunday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
    </script>
@endsection
