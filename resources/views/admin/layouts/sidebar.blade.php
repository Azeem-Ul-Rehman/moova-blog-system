<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    {{--    <a href="{{ asset('admin.home') }}" class="brand-link">--}}
    {{--        <img src="{{ asset('admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"--}}
    {{--             class="brand-image img-circle elevation-3"--}}
    {{--             style="opacity: .8">--}}
    {{--        <span class="brand-text font-weight-light">AdminLTE 3</span>--}}
    {{--    </a>--}}

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ auth()->check() ? auth()->user()->name : '' }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @can('page-list')
                    <li class="nav-item">
                        <a href="{{ route('page.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Pages</p>
                        </a>
                    </li>
                @endcan
                @can('posts-list')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Posts
                                <i class="fas fa-angle-left right"></i>

                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('post.index')}}" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        All Posts
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('post.create') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ADD New</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('category.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>Categories</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('tag.index') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Tags</p>
                                </a>
                            </li>


                        </ul>
                    </li>
                @endcan


                @can('gym-list')
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                GYM
                                <i class="fas fa-angle-left right"></i>

                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('gym.index')}}" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        All Gyms
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('gym.create') }}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>ADD New</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('gym-category.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>Categories</p>
                                </a>
                            </li>


                        </ul>
                    </li>
                @endcan
                @can('instructor-list')
                    <li class="nav-item">
                        <a href="{{ route('instructor.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Instructor
                            </p>
                        </a>
                    </li>
                @endcan

                @can('reviews-list')
                    <li class="nav-item">
                        <a href="{{ route('reviews.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Reviews</p>
                        </a>
                    </li>
                @endcan
                @can('users-list')
                    <li class="nav-item">
                        <a href="{{ route('user.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Users</p>
                        </a>
                    </li>
                @endcan
                @can('role-list')

                    <li class="nav-item">
                        <a href="{{ route('roles.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Roles</p>
                        </a>
                    </li>
                @endcan

                @can('class-list')

                    <li class="nav-item">
                        <a href="{{ route('classes.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Classes</p>
                        </a>
                    </li>
                @endcan

                @can('class-schedule-list')

                    <li class="nav-item">
                        <a href="{{ route('class-schedule.index') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Class Schedule</p>
                        </a>
                    </li>
                @endcan

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

