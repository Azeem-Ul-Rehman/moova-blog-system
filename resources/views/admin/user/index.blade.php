@extends('admin.app')

@section('headSection')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-ticket" aria-hidden="true"></i>Users
                            <small>Create, Read, Update, Delete</small></h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>

                            <li class="breadcrumb-item active"><a href="{{ route('user.index') }}">Users
                                    Table</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Users</h3>
                                @can('users-create')
                                    <a class='float-right btn btn-success' href="{{ route('user.create') }}">Add New</a>
                                @endcan
                            </div>
                        @include('includes.messages')
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Address</th>
                                        <th>Age</th>
                                        <th>Status</th>
                                        <th>Average Rating</th>
                                        <th>Created At</th>
                                        @can('users-edit')
                                            <th>Edit</th>
                                        @endcan
                                        @can('users-delete')
                                            <th>Delete</th>
                                        @endcan
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                        @php
                                            $reviewStars = \App\Models\Review::where('status','approved')->where('user_id',$user->id)->where('type','post')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

                                        @endphp
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                        <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{{ $user->address }}</td>
                                            <td>{{ $user->age }}</td>
                                            <td>{{ $user->status }}</td>
                                            <td>{{ $reviewStars > 0 ? $reviewStars : 0 }}</td>
                                            <td>{{ $user->created_at }}</td>
                                            @can('users-edit')
                                                <td><a href="{{ route('user.edit',$user->id) }}"><span
                                                            class="fa fa-edit"></span></a></td>
                                            @endcan
                                            @can('users-delete')
                                                <td>
                                                    <form id="delete-form-{{ $user->id }}" method="post"
                                                          action="{{ route('user.destroy',$user->id) }}"
                                                          style="display: none">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                    </form>
                                                    <a href="" onclick="
                                                        if(confirm('Are you sure, You Want to delete this?'))
                                                        {
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }
                                                        else{
                                                        event.preventDefault();
                                                        }"><span class="fa fa-trash"></span></a>
                                                </td>
                                            @endcan


                                        </tr>

                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Address</th>
                                        <th>Age</th>
                                        <th>Status</th>
                                        <th>Average Rating</th>
                                        <th>Created At</th>
                                        @can('users-edit')
                                            <th>Edit</th>
                                        @endcan
                                        @can('users-delete')
                                            <th>Delete</th>
                                        @endcan
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->


    </div>
    <!-- /.content-wrapper -->
@endsection

@section('footerSection')
    <!-- jQuery -->
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>
    <script>
        $(function () {
            $('#example2').DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        })
    </script>
@endsection


