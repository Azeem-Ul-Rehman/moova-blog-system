@extends('admin.app')
@section('headSection')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('main-content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            <i class="fa fa-ticket" aria-hidden="true"></i> Class Schedule
                            <small>Create form element</small></h1>

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('class-schedule.index') }}">Class Schedule
                                    Table</a>
                            </li>
                            <li class="breadcrumb-item active">Create From</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create Class Schedule</h3>
                            </div>
                            <!-- /.card-header -->


                            <form role="form" action="{{ route('class-schedule.update',$classSchedule->id) }}"
                                  method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    @include('includes.messages')

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Class Type</label>
                                                <input type="radio" class="form-control" id="class_type"
                                                       name="class_type" value="recurring"
                                                       {{ $classSchedule->class_type == 'recurring' ? 'checked' : '' }}
                                                       placeholder="Class Type">Recurring
                                                <input type="radio" class="form-control" id="class_type"
                                                       name="class_type" value="one_time"
                                                       {{ $classSchedule->class_type == 'one_time' ? 'checked' : '' }}
                                                       placeholder="Class Type">One Time
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="class_id">Select Class</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Class" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="class_id" id="class_id">
                                                    @if(!empty($classes) && count($classes) >0)
                                                        @foreach ($classes as $class)
                                                            <option
                                                                value="{{ $class->id }}" {{ (!is_null($classSchedule->uClass) && $classSchedule->uClass->id == $class->id) ? 'selected' : '' }}>{{ $class->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="start_date">Start Date</label>
                                                <input type="date" class="form-control" id="start_date"
                                                       name="start_date" value="{{ $classSchedule->start_date }}"
                                                       placeholder="Start Date">
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="end_date_recurring"
                                             style="display: {{ ($classSchedule->class_type == 'recurring') ? '' : 'none' }}">
                                            <div class="form-group">
                                                <div id="no_end_Date_checked"
                                                     style="display:{{ (!is_null($classSchedule->no_end_date) && $classSchedule->no_end_date == true) ? 'none' : '0'}};">
                                                    <label for="end_date">End Date</label>
                                                    <input type="date" class="form-control" id="end_date"
                                                           name="end_date"
                                                           value="{{ (!is_null($classSchedule->end_date))? $classSchedule->end_date : '' }}"
                                                           placeholder="End Date">
                                                </div>


                                                <input type="checkbox" class="form-control" id="no_end_date"
                                                       name="no_end_date"
                                                       value="{{ (!is_null($classSchedule->no_end_date) && $classSchedule->no_end_date == true) ? 1 : 0}}"
                                                       {{ (!is_null($classSchedule->no_end_date) && $classSchedule->no_end_date == true) ? 'checked' : '0'}}
                                                       placeholder="No End Date">No End Date
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="instructor_id">Select Instructor</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Instructor" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="instructor_id" id="instructor_id">
                                                    @if(!empty($instructors) && count($instructors) >0)
                                                        @foreach ($instructors as $instructor)
                                                            <option
                                                                value="{{ $instructor->id }}" {{ (!is_null($classSchedule->instructor) && $classSchedule->instructor->id == $instructor->id) ? 'selected' : '' }}>{{ $instructor->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="gym_id">Select Gym</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Gym" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="gym_id" id="gym_id">
                                                    @if(!empty($gyms) && count($gyms) >0)
                                                        @foreach ($gyms as $gym)
                                                            <option
                                                                value="{{ $gym->id }}" {{ (!is_null($classSchedule->gym) && $classSchedule->gym->id == $gym->id) ? 'selected' : '' }}>{{ $gym->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-card">
                                                <div class="form-group">
                                                    <div id="all_contacts">

                                                        @if (isset($classSchedule->classScheduleTime))
                                                            @foreach ($classSchedule->classScheduleTime as $key => $classScheduleTime)

                                                                <div id="emergency_contacts{{$key +1}}">
                                                                    <select
                                                                        class="form-control" id="start_time"
                                                                        name="start_time[]">

                                                                        <option
                                                                            value="01:00" {{ $classScheduleTime->start_time == '01:00:00' ? 'selected' : '' }}>
                                                                            01:00
                                                                        </option>
                                                                        <option
                                                                            value="02:00" {{ $classScheduleTime->start_time == '02:00:00' ? 'selected' : '' }}>
                                                                            02:00
                                                                        </option>
                                                                        <option
                                                                            value="03:00" {{ $classScheduleTime->start_time == '03:00:00' ? 'selected' : '' }}>
                                                                            03:00
                                                                        </option>
                                                                        <option
                                                                            value="04:00" {{ $classScheduleTime->start_time == '04:00:00' ? 'selected' : '' }}>
                                                                            04:00
                                                                        </option>
                                                                        <option
                                                                            value="05:00" {{ $classScheduleTime->start_time == '05:00:00' ? 'selected' : '' }}>
                                                                            05:00
                                                                        </option>
                                                                        <option
                                                                            value="06:00" {{ $classScheduleTime->start_time == '06:00:00' ? 'selected' : '' }}>
                                                                            06:00
                                                                        </option>
                                                                        <option
                                                                            value="07:00" {{ $classScheduleTime->start_time == '07:00:00' ? 'selected' : '' }}>
                                                                            07:00
                                                                        </option>
                                                                        <option
                                                                            value="08:00" {{ $classScheduleTime->start_time == '08:00:00' ? 'selected' : '' }}>
                                                                            08:00
                                                                        </option>
                                                                        <option
                                                                            value="09:00" {{ $classScheduleTime->start_time == '09:00:00' ? 'selected' : '' }}>
                                                                            09:00
                                                                        </option>
                                                                        <option
                                                                            value="10:00" {{ $classScheduleTime->start_time == '10:00:00' ? 'selected' : '' }}>
                                                                            10:00
                                                                        </option>
                                                                        <option
                                                                            value="11:00" {{ $classScheduleTime->start_time == '11:00:00' ? 'selected' : '' }}>
                                                                            11:00
                                                                        </option>
                                                                        <option
                                                                            value="12:00" {{ $classScheduleTime->start_time == '12:00:00' ? 'selected' : '' }}>
                                                                            12:00
                                                                        </option>
                                                                        <option
                                                                            value="13:00" {{ $classScheduleTime->start_time == '13:00:00' ? 'selected' : '' }}>
                                                                            13:00
                                                                        </option>
                                                                        <option
                                                                            value="14:00" {{ $classScheduleTime->start_time == '14:00:00' ? 'selected' : '' }}>
                                                                            14:00
                                                                        </option>
                                                                        <option
                                                                            value="15:00" {{ $classScheduleTime->start_time == '15:00:00' ? 'selected' : '' }}>
                                                                            15:00
                                                                        </option>
                                                                        <option
                                                                            value="16:00" {{ $classScheduleTime->start_time == '16:00:00' ? 'selected' : '' }}>
                                                                            16:00
                                                                        </option>
                                                                        <option
                                                                            value="17:00" {{ $classScheduleTime->start_time == '17:00:00' ? 'selected' : '' }}>
                                                                            17:00
                                                                        </option>
                                                                        <option
                                                                            value="18:00" {{ $classScheduleTime->start_time == '18:00:00' ? 'selected' : '' }}>
                                                                            18:00
                                                                        </option>
                                                                        <option
                                                                            value="19:00" {{ $classScheduleTime->start_time == '19:00:00' ? 'selected' : '' }}>
                                                                            19:00
                                                                        </option>
                                                                        <option
                                                                            value="20:00" {{ $classScheduleTime->start_time == '20:00:00' ? 'selected' : '' }}>
                                                                            20:00
                                                                        </option>
                                                                        <option
                                                                            value="21:00" {{ $classScheduleTime->start_time == '21:00:00' ? 'selected' : '' }}>
                                                                            21:00
                                                                        </option>
                                                                        <option
                                                                            value="22:00" {{ $classScheduleTime->start_time == '22:00:00' ? 'selected' : '' }}>
                                                                            22:00
                                                                        </option>
                                                                        <option
                                                                            value="23:00" {{ $classScheduleTime->start_time == '23:00:00' ? 'selected' : '' }}>
                                                                            23:00
                                                                        </option>
                                                                        <option
                                                                            value="24:00" {{ $classScheduleTime->start_time == '24:00:00' ? 'selected' : '' }}>
                                                                            24:00
                                                                        </option>
                                                                    </select>
                                                                    <input type="number" name="duration[]"
                                                                           placeholder="Duration"
                                                                           value="{{ $classScheduleTime->duration}}">


                                                                    @if($key+1 > 1)
                                                                        <button type="button" name="remove" id="remove"
                                                                                class="btn btn-danger remove">X
                                                                        </button>
                                                                    @else
                                                                        <button type="button" name="add" id="add"
                                                                                class="btn btn-success">ADD More Start
                                                                            Times
                                                                        </button>
                                                                    @endif

                                                                </div>
                                                            @endforeach
                                                        @endif

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="cp_spots">Cp Spots</label>
                                                <select class="form-control"
                                                        data-placeholder="Select a Cp Spots" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="cp_spots" id="cp_spots">

                                                    <option
                                                        value="01" {{ $classSchedule->cp_spots == '01' ? 'selected' : '' }}>
                                                        1
                                                    </option>
                                                    <option
                                                        value="02" {{ $classSchedule->cp_spots == '02' ? 'selected' : '' }}>
                                                        2
                                                    </option>
                                                    <option
                                                        value="03" {{ $classSchedule->cp_spots == '03' ? 'selected' : '' }}>
                                                        3
                                                    </option>
                                                    <option
                                                        value="04" {{ $classSchedule->cp_spots == '04' ? 'selected' : '' }}>
                                                        4
                                                    </option>
                                                    <option
                                                        value="05" {{ $classSchedule->cp_spots == '05' ? 'selected' : '' }}>
                                                        5
                                                    </option>
                                                    <option
                                                        value="06" {{ $classSchedule->cp_spots == '06' ? 'selected' : '' }}>
                                                        6
                                                    </option>
                                                    <option
                                                        value="07" {{ $classSchedule->cp_spots == '07' ? 'selected' : '' }}>
                                                        7
                                                    </option>
                                                    <option
                                                        value="08" {{ $classSchedule->cp_spots == '08' ? 'selected' : '' }}>
                                                        8
                                                    </option>
                                                    <option
                                                        value="09" {{ $classSchedule->cp_spots == '09' ? 'selected' : '' }}>
                                                        9
                                                    </option>
                                                    <option
                                                        value="10" {{ $classSchedule->cp_spots == '10' ? 'selected' : '' }}>
                                                        10
                                                    </option>
                                                    <option
                                                        value="11" {{ $classSchedule->cp_spots == '11' ? 'selected' : '' }}>
                                                        11
                                                    </option>
                                                    <option
                                                        value="12" {{ $classSchedule->cp_spots == '12' ? 'selected' : '' }}>
                                                        12
                                                    </option>
                                                    <option
                                                        value="13" {{ $classSchedule->cp_spots == '13' ? 'selected' : '' }}>
                                                        13
                                                    </option>
                                                    <option
                                                        value="14" {{ $classSchedule->cp_spots == '14' ? 'selected' : '' }}>
                                                        14
                                                    </option>
                                                    <option
                                                        value="15" {{ $classSchedule->cp_spots == '15' ? 'selected' : '' }}>
                                                        15
                                                    </option>
                                                    <option
                                                        value="16" {{ $classSchedule->cp_spots == '16' ? 'selected' : '' }}>
                                                        16
                                                    </option>
                                                    <option
                                                        value="17" {{ $classSchedule->cp_spots == '17' ? 'selected' : '' }}>
                                                        17
                                                    </option>
                                                    <option
                                                        value="18" {{ $classSchedule->cp_spots == '18' ? 'selected' : '' }}>
                                                        18
                                                    </option>
                                                    <option
                                                        value="19 {{ $classSchedule->cp_spots == '19' ? 'selected' : '' }}">
                                                        19
                                                    </option>
                                                    <option
                                                        value="20" {{ $classSchedule->cp_spots == '20' ? 'selected' : '' }}>
                                                        20
                                                    </option>
                                                    <option
                                                        value="21" {{ $classSchedule->cp_spots == '21' ? 'selected' : '' }}>
                                                        21
                                                    </option>
                                                    <option
                                                        value="22" {{ $classSchedule->cp_spots == '22' ? 'selected' : '' }}>
                                                        22
                                                    </option>
                                                    <option
                                                        value="23" {{ $classSchedule->cp_spots == '23' ? 'selected' : '' }}>
                                                        23
                                                    </option>
                                                    <option
                                                        value="24" {{ $classSchedule->cp_spots == '24' ? 'selected' : '' }}>
                                                        24
                                                    </option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="capacity">Capacity</label>
                                                <select class="form-control"
                                                        data-placeholder="Select Capacity" style="width: 100%;"
                                                        tabindex="-1"
                                                        aria-hidden="true" name="capacity" id="capacity">

                                                    <option
                                                        value="01" {{ $classSchedule->capacity == '01' ? 'selected' : '' }}>
                                                        1
                                                    </option>
                                                    <option
                                                        value="02" {{ $classSchedule->capacity == '02' ? 'selected' : '' }}>
                                                        2
                                                    </option>
                                                    <option
                                                        value="03" {{ $classSchedule->capacity == '03' ? 'selected' : '' }}>
                                                        3
                                                    </option>
                                                    <option
                                                        value="04" {{ $classSchedule->capacity == '04' ? 'selected' : '' }}>
                                                        4
                                                    </option>
                                                    <option
                                                        value="05" {{ $classSchedule->capacity == '05' ? 'selected' : '' }}>
                                                        5
                                                    </option>
                                                    <option
                                                        value="06" {{ $classSchedule->capacity == '06' ? 'selected' : '' }}>
                                                        6
                                                    </option>
                                                    <option
                                                        value="07" {{ $classSchedule->capacity == '07' ? 'selected' : '' }}>
                                                        7
                                                    </option>
                                                    <option
                                                        value="08" {{ $classSchedule->capacity == '08' ? 'selected' : '' }}>
                                                        8
                                                    </option>
                                                    <option
                                                        value="09" {{ $classSchedule->capacity == '09' ? 'selected' : '' }}>
                                                        9
                                                    </option>
                                                    <option
                                                        value="10" {{ $classSchedule->capacity == '10' ? 'selected' : '' }}>
                                                        10
                                                    </option>
                                                    <option
                                                        value="11" {{ $classSchedule->capacity == '11' ? 'selected' : '' }}>
                                                        11
                                                    </option>
                                                    <option
                                                        value="12" {{ $classSchedule->capacity == '12' ? 'selected' : '' }}>
                                                        12
                                                    </option>
                                                    <option
                                                        value="13" {{ $classSchedule->capacity == '13' ? 'selected' : '' }}>
                                                        13
                                                    </option>
                                                    <option
                                                        value="14" {{ $classSchedule->capacity == '14' ? 'selected' : '' }}>
                                                        14
                                                    </option>
                                                    <option
                                                        value="15" {{ $classSchedule->capacity == '15' ? 'selected' : '' }}>
                                                        15
                                                    </option>
                                                    <option
                                                        value="16" {{ $classSchedule->capacity == '16' ? 'selected' : '' }}>
                                                        16
                                                    </option>
                                                    <option
                                                        value="17" {{ $classSchedule->capacity == '17' ? 'selected' : '' }}>
                                                        17
                                                    </option>
                                                    <option
                                                        value="18" {{ $classSchedule->capacity == '18' ? 'selected' : '' }}>
                                                        18
                                                    </option>
                                                    <option
                                                        value="19 {{ $classSchedule->capacity == '19' ? 'selected' : '' }}">
                                                        19
                                                    </option>
                                                    <option
                                                        value="20" {{ $classSchedule->capacity == '20' ? 'selected' : '' }}>
                                                        20
                                                    </option>
                                                    <option
                                                        value="21" {{ $classSchedule->capacity == '21' ? 'selected' : '' }}>
                                                        21
                                                    </option>
                                                    <option
                                                        value="22" {{ $classSchedule->capacity == '22' ? 'selected' : '' }}>
                                                        22
                                                    </option>
                                                    <option
                                                        value="23" {{ $classSchedule->capacity == '23' ? 'selected' : '' }}>
                                                        23
                                                    </option>
                                                    <option
                                                        value="24" {{ $classSchedule->capacity == '24' ? 'selected' : '' }}>
                                                        24
                                                    </option>
                                                </select>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="row" id="repeat_on_recurring"
                                         style="display: {{ ($classSchedule->class_type == 'recurring') ? '' : 'none' }}">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Repeat On</label>
                                                <br>
                                                <input type="checkbox" name="monday" id="monday"
                                                       value="{{ (!is_null($classSchedule->monday) && $classSchedule->monday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->monday) && $classSchedule->monday == true) ? 'checked' : '0'}}>Monday<br>
                                                <input type="checkbox" name="tuesday" id="tuesday"
                                                       value="{{ (!is_null($classSchedule->tuesday) && $classSchedule->tuesday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->tuesday) && $classSchedule->tuesday == true) ? 'checked' : '0'}}>Tuesday<br>
                                                <input type="checkbox" name="wednesday" id="wednesday"
                                                       value="{{ (!is_null($classSchedule->wednesday) && $classSchedule->wednesday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->wednesday) && $classSchedule->wednesday == true) ? 'checked' : '0'}}>Wednesday<br>
                                                <input type="checkbox" name="thursday" id="thursday"
                                                       value="{{ (!is_null($classSchedule->thursday) && $classSchedule->thursday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->thursday) && $classSchedule->thursday == true) ? 'checked' : '0'}}>Thursday<br>
                                                <input type="checkbox" name="friday" id="friday"
                                                       value="{{ (!is_null($classSchedule->friday) && $classSchedule->friday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->friday) && $classSchedule->friday == true) ? 'checked' : '0'}}>Friday<br>
                                                <input type="checkbox" name="saturaday" id="saturaday"
                                                       value="{{ (!is_null($classSchedule->saturaday) && $classSchedule->saturaday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->saturaday) && $classSchedule->saturaday == true) ? 'checked' : '0'}}>Saturday<br>
                                                <input type="checkbox" name="sunday" id="sunday"
                                                       value="{{ (!is_null($classSchedule->sunday) && $classSchedule->sunday == true) ? 1 : 0}}"
                                                    {{ (!is_null($classSchedule->sunday) && $classSchedule->sunday == true) ? 'checked' : '0'}}>Sunday<br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="single_date_recurring"
                                         style="display: {{ ($classSchedule->class_type == 'recurring') ? '' : 'none' }}">
                                        <div class="col-md-6">
                                            <div class="form-card">
                                                <div class="form-group">
                                                    <div id="all_contacts_time">
                                                        @if (isset($classSchedule->classScheduleSingleDay))
                                                            @foreach ($classSchedule->classScheduleSingleDay as $key => $classScheduleSingleDay)

                                                                <div id="emergency_contacts_time{{$key +1}}">
                                                                    <input type="date" name="date[]"
                                                                           placeholder="dd/mm/yyyy"
                                                                           value="{{$classScheduleSingleDay->date}}"
                                                                           >

                                                                    @if($key+1 > 1)
                                                                        <button type="button" name="remove1"
                                                                                id="remove1"
                                                                                class="btn btn-danger remove1">X
                                                                        </button>
                                                                    @else
                                                                        <button type="button" name="add1" id="add1"
                                                                                class="btn btn-success">ADD More Single
                                                                            Day
                                                                            Exception
                                                                        </button>
                                                                    @endif

                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="card-footer">
                                    <input type="submit" class="btn btn-primary">
                                    <a href='{{ route('class-schedule.index') }}' class="btn btn-warning">Back</a>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->


                    </div>
                    <!--/.col (left) -->

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


@section('footerSection')
    <!-- jQuery -->
    <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- Bootstrap4 Duallistbox -->
    <script src="{{ asset('admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <!-- Bootstrap Switch -->
    <script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('admin/dist/js/demo.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        // Summernote
        $('.textarea').summernote()

        $('#no_end_date').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);
                $('#no_end_Date_checked').css('display', 'none');
            } else {
                $(this).val(0);
                $('#no_end_Date_checked').css('display', '');
            }
        });

        $("input[name='class_type']").change(function () {
            debugger;
            if ($(this).val() === 'recurring') {
                $('#end_date_recurring').css('display', '');
                $('#repeat_on_recurring').css('display', '');
                $('#single_date_recurring').css('display', '');
            } else {
                $('#end_date_recurring').css('display', 'none');
                $('#repeat_on_recurring').css('display', 'none');
                $('#single_date_recurring').css('display', 'none');
            }

        });
        $(document).ready(function () {
            var count = 1;
            count = {{count($classSchedule->classScheduleTime) > 0 ? count($classSchedule->classScheduleTime) : 1 }}
                // dynamic_field(count);

                function dynamic_field(number) {
                    html = '<div id="emergency_contacts' + number + '">';
                    html += '<select class="form-control" id="start_time" name="start_time[]" >\n' +
                        '\n' +
                        '                                                            <option value="01:00" selected>01:00</option>\n' +
                        '                                                            <option value="02:00">02:00</option>\n' +
                        '                                                            <option value="03:00">03:00</option>\n' +
                        '                                                            <option value="04:00">04:00</option>\n' +
                        '                                                            <option value="05:00">05:00</option>\n' +
                        '                                                            <option value="06:00">06:00</option>\n' +
                        '                                                            <option value="07:00">07:00</option>\n' +
                        '                                                            <option value="08:00">08:00</option>\n' +
                        '                                                            <option value="09:00">09:00</option>\n' +
                        '                                                            <option value="10:00">10:00</option>\n' +
                        '                                                            <option value="11:00">11:00</option>\n' +
                        '                                                            <option value="12:00">12:00</option>\n' +
                        '                                                            <option value="13:00">13:00</option>\n' +
                        '                                                            <option value="14:00">14:00</option>\n' +
                        '                                                            <option value="15:00">15:00</option>\n' +
                        '                                                            <option value="16:00">16:00</option>\n' +
                        '                                                            <option value="17:00">17:00</option>\n' +
                        '                                                            <option value="18:00">18:00</option>\n' +
                        '                                                            <option value="19:00">19:00</option>\n' +
                        '                                                            <option value="20:00">20:00</option>\n' +
                        '                                                            <option value="21:00">21:00</option>\n' +
                        '                                                            <option value="22:00">22:00</option>\n' +
                        '                                                            <option value="23:00">23:00</option>\n' +
                        '                                                            <option value="24:00">24:00</option>\n' +
                        '                                                        </select>';
                    html += '<input type="number" name="duration[]" placeholder="Duration" />';
                    if (number > 1) {
                        html += '<button type="button" name="remove" id="remove" class="btn btn-danger remove">X</button></div>';
                        $('#all_contacts').append(html);
                    } else {
                        html += '<button type="button" name="add" id="add" class="btn btn-success">ADD More Start Times</button></div>';
                        $('#all_contacts').html(html);
                    }
                }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);
            });

            $(document).on('click', '.remove', function () {
                var removecount = count;
                count--;
                $('#emergency_contacts' + removecount + '').remove();
            });


            //    Single Day Exception

            var count1 = 1;
            count1 = {{count($classSchedule->classScheduleSingleDay) > 0 ? count($classSchedule->classScheduleSingleDay) : 1 }}
                // dynamic_field1(count1);

                function dynamic_field1(number) {
                    html = '<div id="emergency_contacts_time' + number + '">';
                    html += '<input type="date" name="date[]" placeholder="dd/mm/yyyy"/>';
                    if (number > 1) {
                        html += '<button type="button" name="remove1" id="remove1" class="btn btn-danger remove1">X</button></div>';
                        $('#all_contacts_time').append(html);
                    } else {
                        html += '<button type="button" name="add1" id="add1" class="btn btn-success">ADD More Single Day Exception</button></div>';
                        $('#all_contacts_time').html(html);
                    }
                }

            $(document).on('click', '#add1', function () {
                count1++;
                dynamic_field1(count1);
            });

            $(document).on('click', '.remove1', function () {
                var removecount1 = count1;
                count1--;
                $('#emergency_contacts_time' + removecount1 + '').remove();
            });
        });


        $('#monday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#tuesday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#wednesday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#thursday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#friday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#saturaday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
        $('#sunday').change(function () {
            if ($(this).is(':checked') === true) {
                $(this).val(1);

            } else {
                $(this).val(0);
            }
        });
    </script>
@endsection
