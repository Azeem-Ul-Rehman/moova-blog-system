<!-- Price Section mobile Carousel -->
<div class="carousel-wrap desk-hide text-center">
    <div class="owl-carousel">
        <div class="item">
            <div class="card-price card-shadow border-0 mb-4">
                <div class="card-lable">
                    <p>FOREVER free</p>
                </div>
                <div class="card-img">
                    <p class="circle"></p>
                    <h2 class="mb-0">Starter</h2>
                    <h3 class="mb-0">$0</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-note">
                    <p>Get your business / services in front of new customers with a free subscription to the Moova
                        directory</p>
                </div>
                <div class="card-body">
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Moova listing</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Link to website Moova <span
                                class="mn">listing</span> </span></p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Login & manage listing </span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Booking calendar</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Up to 100 bookings per <span
                                class="mn">month</span> </span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Take online payments</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Take up to 100 online <span
                                class="mn">payments per month</span> </span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>7% transaction fee</span></p>
                    <a class="btn pricing-btn" href="#">SIGN UP</a>
                </div>
            </div>
        </div>
        <div class="item middle-box">
            <div class="card-pricetwo card-shadow border-0 mb-4">
                <div class="card-lable yearly">
                    <p>2 months free</p>
                </div>
                <div class="card-img monthly">
                    <p class="circle"></p>
                    <h2 class="mb-0">Build</h2>
                    <h3 class="mb-0">$45</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-imgyear yearly">
                    <p class="circle"></p>
                    <h2 class="mb-0">Build</h2>
                    <h3 class="mb-0">$45</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-note">
                    <p>Make running your business easy with the foundations for managing bookings and virtual
                        classes</p>
                </div>
                <div class="addition">
                    <p>In addition to Starter:</p>
                </div>
                <div class="card-body">
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Up to 500 bookings <span
                                class="mn">per month</span> </span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Take up to 500 online<span
                                class="mn"> payments per month </span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>6% transaction fee </span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Apply discounts</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Business performance <span
                                class="mn">reports</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Staff reports</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>1 staff login</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Support centre</span></p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Online / virtual classes</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Booking confirmation</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Facebook / Instagram <span
                                class="mn">booking buttons</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Website booking<span
                                class="mn"> button</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Client reminders</span></p>
                    <a class="btn pricing-btn" href="#">SIGN UP</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card-pricethree card-shadow border-0 mb-4">
                <div class="card-lable">
                    <p>MOST POPULAR</p>
                </div>
                <div class="card-offer yearly">
                    <p>2 months free</p>
                </div>
                <div class="card-img">
                    <p class="circle"></p>
                    <h2 class="mb-0">Ripped</h2>
                    <h3 class="mb-0">$85</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-note">
                    <p>Our most popular plan. Grow your revenue and elevate your business with the powerful Moova
                        marketing suite</p>
                </div>
                <div class="addition">
                    <p>In addition to Build:</p>
                </div>
                <div class="card-body">
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Up to 500 bookings per <span
                                class="mn">month</span> </span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Take up to 500 online<span
                                class="mn"> payments per month </span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>5.5% transaction fee </span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Up to 10 staff logins</span>
                    </p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Dedicated account manager</span>
                    </p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Google analytics integration</span> </span>
                    </p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Facebook pixel integration</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Google tag</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Online training</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Multiple location<span
                                class="mn"> management</span></span></p>
                    <a class="btn pricing-btn" href="#">SIGN UP</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="card-pricefour card-shadow border-0 mb-4">
                <div class="card-lable yearly">
                    <p>2 months free</p>
                </div>
                <div class="card-img monthly">
                    <p class="circle"></p>
                    <h2 class="mb-0">Stacked</h2>
                    <h3 class="mb-0">$150</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-imgyear yearly">
                    <p class="circle"></p>
                    <h2 class="mb-0">Build</h2>
                    <h3 class="mb-0">$45</h3>
                    <h6 class="monthly">monthly</h6>
                    <h6 class="yearly">yearly</h6>
                </div>
                <div class="card-note">
                    <p>Get access to sophisticated tools and 1-on-1 support from the Moova team. Get stacked to grow
                        your business!</p>
                </div>
                <div class="addition">
                    <p>In addition to Ripped:</p>
                </div>
                <div class="card-body">
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Unlimited bookings</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"> <span>Unlimited online<span
                                class="mn"> payments</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>5% transaction fee</span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Up to 25 staff logins</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Website integration<span
                                class="mn"> support</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Rebooking reminders</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Moovas choice (3 days<span
                                class="mn"> per month)</span></span></p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Prompt clients to post<span
                                class="mn"> reviews</span></span></p>
                    <p>
                        <img
                            src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>1-on-1 setup & training</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Moova marketing colab</span>
                    </p>
                    <p><img src="{{ asset('assets/images/icons8-checked-24.png') }}"><span>Branded landing page</span>
                    </p>
                    <a class="btn pricing-btn" href="#">SIGN UP</a>
                </div>
            </div>
        </div>

    </div>

</div>
