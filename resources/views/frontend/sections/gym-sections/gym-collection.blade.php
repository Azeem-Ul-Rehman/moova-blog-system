<!-- gyms Collection Section -->

<section class="gym-collection">
    <div class="container">
        <div class="row pt-5">
            @if(!empty($gyms) && count($gyms) > 0)
                @foreach($gyms as $gym)
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="collection-box te mt-5">
                            <img src="{{asset('storage/images/'.$gym->image) }}">
                            <div class="collection-body">
                                <a href="{{ route('inner-gym.show',[$gym->id]) }}">
                                    <h2>{{ $gym->name }}</h2>
                                </a>
                                <p>{{ $gym->address }}</p>

                                @php
                                    $reviewCount = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->count();
                                    $reviewStars = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

                                @endphp
                                @if($reviewStars > 0 && $reviewStars <=1)
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                @endif
                                @if($reviewStars > 1 && $reviewStars <=2)
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                @endif
                                @if($reviewStars > 2 && $reviewStars <=3)
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                @endif
                                @if($reviewStars > 3 && $reviewStars <=4)
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                @endif
                                @if($reviewStars > 4 && $reviewStars <=5)
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                    <img class="star" src="{{ asset('assets/images/home/star.svg') }}">
                                @endif
                                @if($reviewCount > 0)
                                    <span>  {{$reviewCount}} reviews</span>
                                @endif

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
