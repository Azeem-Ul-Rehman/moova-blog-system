<!-- Section Two Blurb Section -->

<section class="blurb-box">
    <div class="container">
        <div class="row desk-hide">
            <div class="col-12">
                <div class="card-cta">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{ asset('assets/images/home/gym-mbl.png') }}">
                        </div>
                        <div class="col-8 cta-body">
                            <h2>Gyms</h2>
                            <p>Body copy example</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card-cta">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{ asset('assets/images/home/yoga-mbl.png') }}">
                        </div>
                        <div class="col-8 cta-body">
                            <h2>Yoga</h2>
                            <p>Body copy example</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card-cta">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{ asset('assets/images/home/trainer-mbl.png') }}">
                        </div>
                        <div class="col-8 cta-body">
                            <h2>Personal trainers</h2>
                            <p>Body copy example</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mbl-hide">
            <!-- Column One -->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="card">
                    <div class="card-imgs">
                        <img src="{{ asset('assets/images/home/gyms.png') }}">
                    </div>
                    <div class="card-body">
                        <h2>Gyms</h2>
                        <p>Body copy example</p>
                    </div>
                </div>
            </div>
            <!-- Column Two -->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="card">
                    <div class="card-imgs">
                        <img src="{{ asset('assets/images/home/yoga.png') }}">
                    </div>
                    <div class="card-body">
                        <h2>Yoga</h2>
                        <p>Body copy example</p>
                    </div>
                </div>
            </div>
            <!-- Column Three -->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="card">
                    <div class="card-imgs">
                        <img src="{{ asset('assets/images/home/heading.png') }}">
                    </div>
                    <div class="card-body">
                        <h2>Heading goes here</h2>
                        <p>Body copy example</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
