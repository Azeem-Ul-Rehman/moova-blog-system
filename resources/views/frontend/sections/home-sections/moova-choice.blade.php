<!-- Moova Choice Section -->

<section class="moova-choice">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> MOOVAS CHOICE</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem vel
                    tempus. Praesent semper.</p>
            </div>
        </div>

        <!-- Moova Choice Boxs -->

        <div class="mbl-hide">
            <div class="row">
            @if(!empty($gyms) && count($gyms) > 0)
                @foreach($gyms as $index=>$gym)
                    <!-- Column One -->
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card-choice">
                                <div class="row">
                                    <div class="col-4">
                                        <img src="{{ asset('storage/image_thumbnail/'.$gym->image_thumbnail) }}">
                                    </div>
                                    
                                        <div class="col-8">
                                            <div class="choice-dec">
                                                <div class="color-box">
                                                    <h5><img src="{{ asset('assets/images/home/white-log.svg') }}">
                                                        MOOVAS
                                                        CHOICE
                                                    </h5>
                                                </div>
                                                <a href="{{ route('inner-gym.show',[$gym->id]) }}">
                                                <h2>{{ $gym->name }}</h2>
                                                </a>
                                                <p>{{$gym->address}}</p>
                                                @php
                                                    $reviewCount = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->count();
                                                    $reviewStars = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

                                                @endphp
                                                @if($reviewStars > 0 && $reviewStars <=1)
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                @endif
                                                @if($reviewStars > 1 && $reviewStars <=2)
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                @endif
                                                @if($reviewStars > 2 && $reviewStars <=3)
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                @endif
                                                @if($reviewStars > 3 && $reviewStars <=4)
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                @endif
                                                @if($reviewStars > 4 && $reviewStars <=5)
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                    <img src="{{ asset('assets/images/path.png') }}">
                                                @endif
                                                @if($reviewCount > 0)
                                                    <span>  {{$reviewCount}} reviews</span>
                                                @endif
                                            </div>
                                        </div>
                                   
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>
        </div>


        <!----Moova Choice For Mobile Box-->


        <div class="mobile-carousel">
            <div class="carousel-wrap desk-hide">
                <div class="owl-carousel">
                    @if(!empty($gyms) && count($gyms) > 0)
                        @foreach($gyms as $index=>$gym)
                            <div class="item">
                                <div class="mbl-choice">
                                    <img src="{{ asset('assets/images/home/choice-img.png') }}">
                                    <div class="mblchoice-dec">
                                        <div class="bg-box">
                                            <img src="{{ asset('assets/images/home/white-log.svg') }}"> <h5> MOOVAS
                                                CHOICE</h5>
                                        </div>
                                        <h3>{{ $gym->name }}</h3>
                                        <p>{{$gym->address}}</p>
                                        <div class="star-img">
                                            @php
                                                $reviewCount = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->count();
                                                $reviewStars = \App\Models\Review::where('type_id',$gym->id)->where('status','approved')->where('type','gym')->selectRaw('SUM(rating)/COUNT(user_id) AS avg_rating')->first()->avg_rating;

                                            @endphp
                                            @if($reviewStars > 0 && $reviewStars <=1)
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                            @endif
                                            @if($reviewStars > 1 && $reviewStars <=2)
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                            @endif
                                            @if($reviewStars > 2 && $reviewStars <=3)
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                            @endif
                                            @if($reviewStars > 3 && $reviewStars <=4)
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                            @endif
                                            @if($reviewStars > 4 && $reviewStars <=5)
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                                <img src="{{ asset('assets/images/home/star.svg') }}">
                                        @endif
                                        <!-- <span>  193 reviews</span> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>
        </div>

        <div class="all-studio mbl-hide">
            <a class="btn" href="{{ route('user.gym.index') }}" type="submit">Explore Studios</a>
        </div>

    </div>
</section>
