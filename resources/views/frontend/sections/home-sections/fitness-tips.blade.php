<!-- Fitness Test Section -->

<section class="fitness">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> FITNESS TIPS</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem vel
                    tempus. Praesent semper.</p>
            </div>
        </div>
    </div>

    <div class="carousel-wrap">
        <div class="owl-carousel">
        @if(!empty($posts) && count($posts) > 0)
            @foreach($posts as $post)
            <div class="item post">
                    <img src="{{asset('storage/images/'.$post->image) }}" alt="Owl Image">
                    <p>The Latest <span>{{ date('d-m-Y',strtotime($post->created_at))}}</span></p>
                    <a href="{{ route('single-blog.show',[$post->id]) }}">{{ \Illuminate\Support\Str::limit($post->title,20) }}</a>
                </div>
            @endforeach
        @endif
           
            <!-- <div class="item post">
                <img src="{{ asset('assets/images/home/blog2.png') }}" alt="Owl Image">
                <p>The Latest <span>Mar 25th, 2020</span></p>
                <h2>Blog Post Title 1</h2>
            </div>
            <div class="item post">
                <img src="{{ asset('assets/images/home/blog3.png') }}" alt="Owl Image">
                <p>The Latest <span>Mar 25th, 2020</span></p>
                <h2>Blog Post Title 1</h2>
            </div> -->

        </div>

    </div>




    <div class="all-studio">
        <a  href="{{ route('blog.index') }}" class="btn" type="submit">ALL TIPS</a>
    </div>

</section>
