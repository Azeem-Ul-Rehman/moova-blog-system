<!-- gyms Collection Section -->
<style>
    span.relative.z-0.inline-flex.shadow-sm.rounded-md {
    display: none;
}

</style>

<section class="gym-collection">
    <div class="container">
        <div class="row ">
            @if(!empty($posts) && count($posts) > 0)
                @foreach($posts as $post)
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                    <div class="blog-post pt-5">
                    <img  src="{{asset('storage/images/'.$post->image) }}" >
                    <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-6 ">
                    <p class="ml-4">The Latest</p>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 ">
                    <p>{{ date('d-m-Y',strtotime($post->created_at))}}</p>
                    </div>

                        </div>
                    <a class="ml-4" href="{{ route('single-blog.show',[$post->id]) }}">{{ \Illuminate\Support\Str::limit($post->title,20) }}</a>
                </div>

                    </div>
                @endforeach
            @endif

        </div>

       
        <div class="row text-center mt-5">
            <div class="col-md-12">
            {{ $posts->links() }}
            </div>
        </div>

    </div>


</section>

