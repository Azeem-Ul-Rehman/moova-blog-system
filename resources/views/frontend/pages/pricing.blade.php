@extends('frontend.layouts.app')
@section('content')

    <!-- Price Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> PRICING</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                    <h4 class="mbl-hide">We're offering a 30 day free trial (no credit card required)</h4>
                </div>
            </div>
        </div>
    </section>

    <!-- Price Section -->
    <section class="price">

        <div class="pricing5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <div class="switcher-box mt-4">
                            <div class="btn-group rounded-pill" data-toggle="buttons">
                                <label class="btn btn-outline-secondary btn-md togl-button active ">
                                    <input type="radio" name="options" id="option1" checked> Monthly
                                </label>
                                <label class="btn btn-outline-secondary togl-button  btn-md">
                                    <input type="radio" name="options" id="option2"> Yearly
                                </label>
                            </div>
                        </div>
                        <div class="price-note mbl-hide">
                            <p>2 months free with a yearly plan</p>
                        </div>
                    </div>
                </div>


            @include('frontend.sections.price-sections.price-mobile')




            <!-- Price Section Desktop Sections -->
    @include('frontend.sections.price-sections.price-desktop')





@endsection
