@extends('frontend.layouts.app')
@section('content')

    <!-- join us Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> JOIN US</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                </div>
            </div>
        </div>
    </section>


    <!-- join us Card Section -->

    <section class="join-card">
        <div class="container">
            <div class="row">
                <div class="join-body text-center">
                    <h2>Sign up</h2>
                    <div class="email-box">
                        <a href="{{ route('register.create') }}">
                            <h3>SIGN UP WITH EMAIL</h3>
                        </a>

                    </div>
                    <div class="google-box">
                        <h3><img
                                src="{{ asset('assets/images/icons8-google-48.png') }}"><span>CONTINUE WITH GOOGLE</span>
                        </h3>
                    </div>
                    <div class="facebook-box">
                        <h3><img src="{{ asset('assets/images/icons8-facebook-f-30.png') }}"><span>CONTINUE WITH FACEBOOK</span>
                        </h3>
                    </div>

                    <p>Already have an account?</p>
                    <a href="{{ route('login.create') }}"><h5><u>Log in</u></h5></a>
                </div>

                <div class="partner-card text-center">
                    <h2>Become a partner</h2>
                    <p>Manage your business with Moova by<u> signing up as a professional.</u></p>
                </div>


            </div>
        </div>
    </section>


    <!-- Footer Section -->

@endsection
