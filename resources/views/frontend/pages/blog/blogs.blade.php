@extends('frontend.layouts.app')
@section('content')

    <!-- Blog Header Section -->

    <section class="blog-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    
                    
                </div>
            </div>
        </div>
    </section>

   <section class="featured-blog">
       <div class="container">
           <div class="row">
               
         
               @if(!is_null($featuredPost))
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
               <img class="featured-img" src="{{asset('storage/images/'.$featuredPost->image) }}">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
               <div class="featured-body">
                   <div class="row">
                       <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-6 ">
                           <p>Featured</p>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 ">
                           <p>{{ date('d-m-Y',strtotime($featuredPost->created_at))}}</p>
                            </div>
                    </div>

                    <a class="ml-4" href="{{ route('single-blog.show',[$featuredPost->id]) }}"> 
                        <h2>{!! $featuredPost->title !!}</h2> </a>
                </div>
               @endif
               



                
    </div>
    </div>
   </section>
    

    @include('frontend.sections.blog-sections.blog-collection')

@endsection
