@extends('frontend.layouts.app')
@section('content')

    <!-- How it works Section -->

    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> BLOG DETAIL</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-detail">
        <div class="container">
            <div class="row">
            </div>
            <img src="{{asset('storage/images/'.$post->image) }}">


            <h2 class="pt-4">{{ \Illuminate\Support\Str::limit($post->title,20) }}</h2>
            <p>Posted By:  {{ $post->user_name }} at {{ date('d-m-Y',strtotime($post->updated_at))}}</p>
{{--            <p>{{ date('d-m-Y',strtotime($post->created_at))}}</p>--}}
            <p>{!!($post->body)!!}</p>


            @if(!empty($categories) && count($categories) > 0)
                <div class="related">
                    <h5>Categories</h5>
                    <div class="row">
                        @foreach($categories as $index=>$category)
                            <a href="{{ route('blog.category',[$category->id])}}">
                                <div class="related-box">
                                    <div class="col-md-2">
                                        {{($category->name)}}
                                        @if($index < count($categories) -1)

                                        @endif
                                    </div>

                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif


            @if(!empty($tags) && count($tags) > 0)
                <div class="related">
                    <h5>Tags</h5>
                    <div class="row">
                        @foreach($tags as $index=>$tag)
                            <a href="{{ route('blog.tag',[$tag->id])}}">
                                <div class="related-box">
                                    <div class="col-md-2">
                                        {{($tag->name)}}

                                        @if($index < count($tags) -1)

                                        @endif
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif

        </div>
    </section>





@endsection
