@extends('frontend.layouts.app')
@section('content')

    <!-- Explore Section Section -->

    <section class="explore">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> EXPLORE</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                </div>
            </div>

            <!-- Trainer Boxes -->

            <div class="explore-boxs">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="sports-box">
                            <img src="{{ asset('assets/images/explore/gym-studio.png') }}">
                            <h2>Gyms / Studios</h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="sports-box">
                            <img src="{{ asset('assets/images/explore/yoga-trainer.png') }}">
                            <h2>Yoga Instructors</h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="sports-box">
                            <img src="{{ asset('assets/images/explore/personal-trainer.png') }}">
                            <h2>Personal Trainers</h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="sports-box">
                            <img src="{{ asset('assets/images/explore/water-sports.png') }}">
                            <h2>Watersports</h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>



@endsection
