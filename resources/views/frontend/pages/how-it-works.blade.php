@extends('frontend.layouts.app')
@section('content')

    <!-- How it works Section -->

    <section class="how-work">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> HOW IT WORKS</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                </div>
            </div>
        </div>
    </section>


@endsection
