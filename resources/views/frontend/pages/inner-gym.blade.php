@extends('frontend.layouts.app')
@section('content')

    <!--inner page-header -->

    <div class="inner-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @include('includes.messages')
                </div>
            </div>
        </div>
    </div>

    <!-- news -->
    <div class="card-section">
        <div class="container text-center">
            <div class="card-block bg-white mb30">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- section-title -->
                        <div class="text-center mb-0">
                            <h2>{{ $gym->name }}</h2>
                            <p>{{ $gym->address }}</p>
                        </div>
                        <!-- /.section-title -->
                        <!-- section-star -->
                        <div class="ratig text-center">
                            @if($reviewStars > 0 && $reviewStars <=1)
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 1 && $reviewStars <=2)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 2 && $reviewStars <=3)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 3 && $reviewStars <=4)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif
                            @if($reviewStars > 4 && $reviewStars <=5)
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                                <img src="{{ asset('assets/images/path.png') }}">
                            @endif


                            @if($reviewCount > 0)
                                <span>{{ $reviewCount }} reviews</span>
                            @endif
                        </div>
                        <!-- /.section-start -->
                        <button class="btn join mt-4" type="submit">BOOK NOW</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- /.news -->

    <!----Description Section-->

    <div class="discription-section">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                    <h2>Description</h2>
                    <p>{!! $gym->description !!}</p>

                    <h3>Gym staff</h3>

                    <div class="row mt-5 ">
                        <div class="col-xl-6 col-lg-5 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row spc">
                        <div class="col-xl-6 col-lg-5 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="row spc">
                        <div class="col-xl-6 col-lg-5 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card-staff">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                        <img
                                            src="{{ asset('assets/images/inner-gym/73322908-D122-441D-8685-1150EB3C30BF.png') }}">
                                    </div>
                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 mt-3">
                                        <h5>Full name</h5>
                                        <p>Yoga</p>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>


                    @include('frontend.sections.class-section')


                    <div class="revw-sec">

                        @if(!empty($reviews))
                            @foreach($reviews as $key=>$review)
                                <div class="row @if($key == 0 ) mt-5 @endif">
                                    @if($key == 0)
                                        <h2>Reviews</h2>
                                    @endif

                                </div>
                                <div class="row">
                                    <p>{{ $review->description }} </p>

                                </div>
                                <div class="row">
                                    <p>{{ $review->title }} </p>
                                    <p>
                                        @if($review->rating > 0 && $review->rating <=1)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 1 && $review->rating <=2)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 2 && $review->rating <=3)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 3 && $review->rating <=4)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                        @if($review->rating > 4 && $review->rating <=5)
                                            <img class="ml-3" src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                            <img src="{{ asset('assets/images/path.png') }}">
                                        @endif
                                    </p>
                                </div>
                                <div class="row">
                                    <p>{{ date('d-m-Y', strtotime($review->created_at)) }}</p>
                                </div>
                                <hr class="hr-review">
                            @endforeach
                        @endif

                    </div>


                    <!-- Button trigger modal -->
                    <a type="button" class="btn rev-btn mt-5" data-toggle="modal" data-target="#exampleModal">
                        Give Review
                        </a>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Review</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('review.store') }}" method="post">
                                    @csrf
                                    <div class="modal-body">


                                        <input type="hidden" name="user_id"
                                               value="{{ auth()->check() ? auth()->user()->id : '' }}">
                                        <input type="hidden" name="type_id" value="{{ $gym->id}}">
                                        <input type="hidden" name="type" value="gym">

                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control"
                                                   id="title">
                                        </div>


                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea class="form-control" id="description"
                                                      name="description" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="rating">Rating</label>

                                            <br>
                                            <div class="rate">
                                                <input type="radio" id="star5" name="rating" value="5"/>
                                                <label for="star5" title="text">5 stars</label>
                                                <input type="radio" id="star4" name="rating" value="4"/>
                                                <label for="star4" title="text">4 stars</label>
                                                <input type="radio" id="star3" name="rating" value="3"/>
                                                <label for="star3" title="text">3 stars</label>
                                                <input type="radio" id="star2" name="rating" value="2"/>
                                                <label for="star2" title="text">2 stars</label>
                                                <input type="radio" id="star1" name="rating" value="1"/>
                                                <label for="star1" title="text">1 star</label>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="clearfix"></div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Add
                                            Review
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /col-8 -->
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                    <div class="card-map">
                        <h2>
                            Location
                        </h2>
                    </div>

                    <div class="amenities mt-5 text-center">
                        <h2>
                            Amenities
                        </h2>
                        <div class="row ml-3 text-center">
                            <div class="col-md-2 col-sm-2 col-2">
                                <img style=" width: 30px;" src="https://img.icons8.com/metro/52/000000/shower.png"/>
                                <span class="icon-title">Shower</span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-2"><img style="    width: 30px;"
                                                                      src="https://img.icons8.com/ios/50/000000/school-locker.png"/>
                                <span class="icon-title">Lockers</span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-2"><img style="margin-top: -14px;width: 30px;"
                                                                      src="https://img.icons8.com/pastel-glyph/64/000000/sleeping-mat--v1.png"/>
                                <span class="icon-title">Mats</span>
                            </div>

                            <div class="col-md-2 col-sm-2 col-2"><img style="margin-top: -14px; width: 30px;"
                                                                      src="https://img.icons8.com/pastel-glyph/64/000000/towel.png"/>
                                <span class="icon-title">Towels</span>
                            </div>
                            <div class="col-md-2 col-sm-2 col-2"><img style="    width: 30px;"
                                                                      src="https://img.icons8.com/metro/26/000000/garage.png"/>
                                <span class="icon-title">Parking</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-social mt-5">
                        <h2>
                            Social
                        </h2>
                        <img class="social-img" src="{{ asset('assets/images/inner-gym/insta.svg') }}">
                        <img class="social-img" src="{{ asset('assets/images/inner-gym/twiter.svg') }}">
                        <img class="social-img" src="{{ asset('assets/images/inner-gym/fb.svg') }}">
                        <img class="social-img" src="{{ asset('assets/images/inner-gym/pn.svg') }}">


                    </div>
                </div>
                <!-- /row --></div>
        </div>
    </div>


    <!-- Footer Section -->

@endsection
<style>
    * {
        margin: 0;
        padding: 0;
    }

    .rate {
        float: left;
        height: 46px;
        padding: 0 10px;
    }

    .rate:not(:checked) > input {
        position: absolute;
        top: -9999px;
    }

    .rate:not(:checked) > label {
        float: right;
        width: 1em;
        overflow: hidden;
        white-space: nowrap;
        cursor: pointer;
        font-size: 30px;
        color: #ccc;
    }

    .rate:not(:checked) > label:before {
        content: '★ ';
    }

    .rate > input:checked ~ label {
        color: #ffc700;
    }

    .rate:not(:checked) > label:hover,
    .rate:not(:checked) > label:hover ~ label {
        color: #deb217;
    }

    .rate > input:checked + label:hover,
    .rate > input:checked + label:hover ~ label,
    .rate > input:checked ~ label:hover,
    .rate > input:checked ~ label:hover ~ label,
    .rate > label:hover ~ input:checked ~ label {
        color: #c59b08;
    }

    /* Modified from: https://github.com/mukulkant/Star-rating-using-pure-css */
</style>

