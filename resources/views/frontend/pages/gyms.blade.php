@extends('frontend.layouts.app')
@section('content')

    <!-- gyms Header Section -->

    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 section-heading">
                    <h2><img src="{{ asset('assets/images/home/section-logo.svg') }}"> GYMS</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus<br> malesuada consectetur lorem
                        vel
                        tempus. Praesent semper.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- gyms Tabs Section -->

    <section class="gyms-tabs">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="tabs">
                        <center>
                            <p>
                                <a class="btn desk-hide" data-toggle="collapse" href="#collapseExample" role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    FILTER
                                </a>
                            </p>
                            <p class="mbl-hide">
                                <a class="btn" data-toggle="collapse" href="#collapseExample" role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    Fitness type
                                </a>
                                <a class="btn" data-toggle="collapse" href="#collapseExample" role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    Distance
                                </a>
                                <a class="btn" data-toggle="collapse" href="#collapseExample" role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    Amenities
                                </a>
                            </p>
                        </center>
                        <div class="collapse" id="collapseExample">
                            <div class="content">
                                <div class="row">
                                    @if(!empty($categories) && count($categories) > 0)
                                        @foreach($categories as $category)

                                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6">
                                                <div class="tab-result">
                                                    <div class="circle"></div>
                                                    <a href="{{ route('gym.category',[$category->id])}}">
                                                        <h2>{{($category->name)}}</h2>

                                                    </a>
                                                </div>
                                            </div>


                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.sections.gym-sections.gym-collection')

@endsection
